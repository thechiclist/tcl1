

$(function(){

// init elements
var DELAY = 4000;

var articleArr = $('.article');		// array of <article> (left panel + main image)
var listItemArr = $('.listItem');	// array of <tr> for the main list
var linkArr = $('.article .articleBillet>a');	// array of <a> to billets pages

var currArticle = undefined;
var lastArticle = undefined;
var isAnimating = false;

// categories/colors
var catColorArr = [
			{cat:'Boire & manger',			color:'#B21410'},
			{cat:'Dormir',					color:'#F22600'},
			{cat:'Être solidaire',			color:'#E5662B'},
			{cat:'Faire du shopping',		color:'#B12553'},
			{cat:'Prendre soin de soi',		color:'#2A9D1F'},
			{cat:'Rester chez soi',			color:'#804F82'},
			{cat:'S\'occuper des enfants',	color:'#818806'},
			{cat:'S\'organier',				color:'#D8A003'},
			{cat:'Sortir',					color:'#F22165'},
			{cat:'Voir & apprendre',		color:'#384AA6'}];


// add links to itemlist
listItemArr.on('click', function(evt)
{
	doClick(evt);
});

// double click
listItemArr.on('dblclick', function(evt)
{
	doDoubleClick(evt);
});

// hide all articles
$('.article').css({'position':'absolute', 'left':'980px'});


// start from a billet page
var itemNumber = getUrlVars()['item'];
if (itemNumber != undefined)
{
	var item = parseInt(itemNumber, 10);
	displayArticle(item, false);
	startAnimation();
}
else
{
	// start animation with item 0
	displayArticle(0, false);
	startAnimation();
}




// functions --------------------------------------------------------------------------------

function getUrlVars() // var id = getUrlVars()["id"];
{
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value)
	{
        vars[key] = value;
    });
    return vars;
}

function displayArticle(index, animate)
{
	if (index == lastArticle) return;
	
	// move out article if one is selected
	if (lastArticle != undefined) {
		articleArr.eq(lastArticle).stop().animate({'left':'-980px'}, 1000, 'easeInOutExpo');

		// deselect item list
		//listItemArr[lastArticle].setAttribute('class', 'listItem');
	}
	if (animate)
	{
		articleArr.eq(index).css('left', '980px');
		articleArr.eq(index).stop().animate({'left':'0px'}, 1000, 'easeInOutExpo', function()
		{
			if (isAnimating) startAnimation();
		});
	}
	else
	{
		$('.article').eq(index).css('left', '0px');
	}
	
	// change class to activated in list
	deselectAllItemList();
	selectItemList(index);
	
	currArticle = index;
	lastArticle = index;
}

function deselectAllItemList()
{
	listItemArr.css('background-color', '#efefef');	// reset back the color to light grey
	listItemArr.find('td:first-child').css('background-color', '#CFCECE');	// colorize as well 1st cell of row
	listItemArr.removeClass('listItem_activated');
}

function selectItemList(index)
{
	listItemArr.eq(index).addClass('listItem_activated');
	
	// get the class name of the <tr> cat[1..10]
	if (listItemArr.eq(index).hasClass('cat1') ||
		listItemArr.eq(index).hasClass('cat2') ||
		listItemArr.eq(index).hasClass('cat3') ||
		listItemArr.eq(index).hasClass('cat4') ||
		listItemArr.eq(index).hasClass('cat5') ||
		listItemArr.eq(index).hasClass('cat6') ||
		listItemArr.eq(index).hasClass('cat7') ||
		listItemArr.eq(index).hasClass('cat8') ||
		listItemArr.eq(index).hasClass('cat9') ||
		listItemArr.eq(index).hasClass('cat10'))
	{
		//var categoryName = listItemArr.eq(index).attr('class').match(/cat[0-1]?[0-9]/);
		var categoryIndex = parseInt(listItemArr.eq(index).attr('class').match(/[0-1]?[0-9]/), 10) -1;
		
		var color = catColorArr[categoryIndex].color;
		
		listItemArr.eq(index).css('background-color', color);
		listItemArr.eq(index).find('td:first-child').css('background-color', color);	// colorize as well 1st cell of row
	}
}

 
function startAnimation()
{
	isAnimating = true;
	currArticle++;
	if (currArticle > 6) currArticle = 0;
	setTimeout(function()
	{
		displayArticle(currArticle, true);
	}, DELAY);
}

function stopAnimation()
{
	isAnimating = false;
}

function doClick(evt)
{
	// find which item list has been clicked
	for (var i=0, len=listItemArr.length; i<len; i++)
	{
		if ($(evt.target).parent().is(listItemArr.eq(i)))
		{
			var index = i;
			break;
		}
	}
	stopAnimation();
	displayArticle(index, true);
}

function doDoubleClick(evt)
{
	// find which item list has been clicked
	for (var i=0, len=listItemArr.length; i<len; i++)
	{
		if ($(evt.target).parent().is(listItemArr.eq(i)))
		{
			var index = i;
			break;
		}
	}
	stopAnimation();
	
	// open link
	var url = linkArr.eq(index).attr('href');
	window.open(url, '_self');
}



});