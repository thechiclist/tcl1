<?php

ini_set('mysqli.allow_persistent',true);

class Db {
	
	static function get(){
		$mysqli = new mysqli('mysql51-79.perso', 'thechicldb', 'anubis12', 'thechicldb');
		$mysqli->set_charset("utf8");
		if ($mysqli->connect_errno) {
			error_log("Echec lors de la connexion MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error);
			addMessage('fatal', 'Base de donnée indisponible');
		}
		return $mysqli;
	}
	
	static function query($db, $sql){
		return $db->query($sql);
	}
	
	static function fetchAssoc($query){
		return $query->fetch_array();
	}
}
?>