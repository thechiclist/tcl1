<?php //Úτƒ-8 encoded

include('conf.php');

function __autoload($class)
{
	static $classDir = 'classes';
	$file = str_replace('\\', DIRECTORY_SEPARATOR, ltrim($class, '\\')) . '.php';
	require "$classDir/$file";
}

function includeLoc($file){
	include(loc() . '/' .  $file);
}

function loc(){
	$loc = null;
	if(isset($_REQUEST['locale'])){
		$loc = $_REQUEST['locale'];
	}
	if(!$loc && isset($_SESSION['locale'])){
		$loc = $_SESSION['locale'];
	}
	if(!$loc){
		$loc = 'fr';
	}
	return $loc;
}
?>