<?php

/** Base class for presistance layer
 * An object is saved in a table with the same name of the class name
 */
class User extends Mutable {
	
	function getColumns(){
		return array('id', 'name', 'pwdAlgorithm', 'password', 'role', 'status', 'updated', 'last_connected',
						'created', 'connection_serie', 'connection_token', 'locale', 'adminNote', 'specifics');
	}
	
	function getDateColumns(){
		return array('updated','created','last_connected');
	}
	
	function getReferences(){
		return array(
				'person' => 'person', 'user' => 'user');
	}

	function ownerField(){
		return 'id';
	}
}
$p = new User();
$p->register();
?>
