<?php

class Email {
	
	static function protectEmail( $name ) {
		return( str_replace(array("%0a", "%0d", "Content-Type:",
			"bcc:","to:","cc:","To:","Cc:","Bcc:" ), "", $name ) );
	}
	
	static function sendToAdmin($replyTo, $subject, $text){
		$to = 'pierre.wargnier@bonnefacture.com';
		$headers = 'From: BonneFacture<cgi-mailer@kundenserver.de>'; //Not possible to use another from address
		
		if($replyTo && $replyTo!=''){
			$headers .= "\r\nReply-To: ".protectEmail($replyTo);
		}
		
		if(!mail($to, protectEmail($subject), protectEmail($text), $headers)){
			trigger_error("L'envoi du message à échoué", E_USER_ERROR);
		}
	}
}
?>