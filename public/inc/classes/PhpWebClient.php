<?php

/** 
 * Responsible for JSON Web service
 */
class PhpWebClient extends AbstractClient{
	
	function start() {
		parent::start();
		$GLOBALS['response'] = array();
		$GLOBALS['response']['session'] = array();
		$GLOBALS['response']['session']['messages'] = array();
	}
	
	function output(){
		//header('Content-Type: text/html; charset=utf-8');
		//JsonWs::copyFilter($_SESSION, $GLOBALS['response']['session'], array('userId','connected','userName','locale'));
		//print json_encode($GLOBALS['response'], JSON_NUMERIC_CHECK);
	}
	
	function addMessage($level, $key, $params = null){
		$mess = array('level' => $level, 'key' => $key);
		if($params){
			$mess['params'] = $params;
		}
		if(!isset($GLOBALS['response'])){
			$GLOBALS['response'] = array();
		}
		if(!isset($GLOBALS['response']['session'])){
			$GLOBALS['response']['session'] = array();
		}
		if(!isset($GLOBALS['response']['session']['messages'])){
			$GLOBALS['response']['session']['messages'] = array();
		}
		array_push($GLOBALS['response']['session']['messages'], $mess);
	}
}
?>
