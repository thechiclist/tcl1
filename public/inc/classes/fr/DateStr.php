<?php
define('TODAY', 'AUJOURD\'HUI');
define('TOMORROW', 'DEMAIN');

function dateFormatTitle($dateObj){
	$month = array('janvier','février','mars','avril','mai','juin','juillet','août','septembre','octobre','novembre','décembre');
	return 'le ' .$dateObj['day'] . ' ' . $month[$dateObj['month']-1];
}
?>
