<?php

class DateUtils {
	
	static function getDateStr($date){
		global $lang;
		
		$today = date('Y-m-d');
		
		include_once $lang.'/DateStr.php';
		
		if($date == $today){
			return TODAY;
		}
		
		$tomorrow = date('Y-m-d', time() + 86400);
		if($date == $tomorrow ){
			return TOMORROW;
		}
		$dateObj = date_parse_from_format('Y-m-d', $date);
		
		return dateFormatTitle($dateObj);
	}
	
	static function getDateStrObj($obj, $field){
		if(isset($obj[$field])){
			return DateUtils::getDateStr($obj[$field]);
		}
		return '';
	}
}
?>
