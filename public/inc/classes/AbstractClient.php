<?php

/** 
 * Represent a client (Web, WebService, PDF gen, batch).
 */
abstract class AbstractClient {
	
	var $dbInst;
	
	function start(){
		$GLOBALS['client'] = $this;
	}
	
	static function get(){
		return $GLOBALS['client'];
	}
	
	function is_auth(){
		return isset($_SESSION['userId']);
	}
	
	abstract function addMessage($level, $key, $params = null);
	
	static function copyFilter($src, &$dest, $filter){
		foreach ($filter as $key) {
			if(isset($src[$key])){
				$value = $src[$key];
				if($value != null){
					$dest[$key] = $value;
				}
			}else{
				unset($src[$key]);
			}
		}
	}
	
	function getDb(){
		if(!$this->dbInst){
			$this->dbInst= Db::get();
		}
		return $this->dbInst;
	}
	
	function execQuery($query, $errMsg='Erreur de requete'){
		$db = $this->getDb();
		$res = $db->query($query);
		if(!$res) {
			error_log("Query failure: " . $query. " (" . $db->connect_errno . ") " . $db->connect_error);
			$this->addMessage('error', $errMsg);
		}
		return $res;
	}
	
	function getLastInsertId(){
		$query = "SELECT LAST_INSERT_ID();";
		$res = $this->execQuery($query);
		if($res){
			$row = $res->fetch_row();
			return $row[0];
		}
		return null;
	}
	
	function getSafeAttr($src, $name){
		return isset($src[$name]) ? $this->getDb()->escape_string($src[$name]) : null;
	}
}
?>
