<?php

class CategoryTree {
	
	var $client;
	var $rootId;
	var $lang;
	var $level = 0;
	var $liClasses;
	var $ulClasses;
	
	function CategoryTree($rootId, $lang){
		$this->rootId = $rootId;
		$this->lang = $lang;
	}
	
	function printIt(){
		$this->client = AbstractClient::get();
		$this->printBranch($this->rootId, $this->getUlClass());
	}
	
	function getUlClass(){
		return isset($this->ulClasses[$this->level]) ? ' class="'.$this->ulClasses[$this->level].'"' : '';
	}
	
	function printBranch($parentId, $ulClass){
		if(isset($parentId)){
			$res = $this->client->execQuery('select * from category where status=\'online\' and parent_cat='.$parentId. ' order by rank');
			if($res && $res->num_rows>0){
				$this->level++;
				echo '<ul'.$ulClass.">";
				$liClass = isset($this->liClasses[$this->level-1]) ? ' class="'.$this->liClasses[$this->level-1].'"' : '';
				while($cat = Db::fetchAssoc($res)){
					$id = $cat['id'];
					$label = $cat['label_'.$this->lang];
					echo '<li'.$liClass.'><a href="#" id="'.$id.'">'.$label.'</a>';
					$this->printBranch($id, $this->getUlClass());
					echo '</li>';
				}
				echo "</ul>";
				$this->level--;
			}
		}
		return null;
	}
}
?>