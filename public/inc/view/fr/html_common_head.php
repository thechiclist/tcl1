<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta charset="utf-8">

<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Rosario"/>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Antic"/>
<link href="css/css.css" rel="stylesheet" type="text/css">
<link href="css/css1.css" rel="stylesheet" type="text/css">
<link href="css/main.css" rel="stylesheet" type="text/css">
<link href="css/menus.css" rel="stylesheet" type="text/css">

<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" />
<link rel="icon" type="video/x-mng" href="img/favicon.mng" /> 