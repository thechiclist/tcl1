<!DOCTYPE html>
<html>
<head>
<?php include('html_common_head.php') ?>
<title>The Chic List - </title>
</head>
<body>

<div id="mainWrapper">

<?php include('header.php') ?>

<section>
	<?php foreach($items as $doc){ ?>
	<article class="article" style="position: absolute; left: -980px;">
    	<div class="articleLeft">
      		<p class="articleSticker"><a href="index.php"><?php echo $doc['chic_clics'];?> chic clics</a></p>
       		<p class="text1"><?php echo $doc['exp_label'];?></p>
            <p class="text2"><?php echo isset($doc['when_txt']) ? $doc['when_txt'] : 'quand on veut';?></p>
            <p class="text3"><?php echo $doc['title'];?></p>
            <p class="text4"><?php echo $doc['subtitle'];?></p>
            <div class="articleBillet"><a href="list-<?php echo $list['id'] ?>-doc-<?php echo $doc['target'] ?>.html" target="_self">Chic Billet</a></div>
            <!-- <div class="articleBook"><a href="index.php">Chic Book</a></div> -->
        </div>
    	<div class="articleRight">
        	<img src="imgBase/<?php echo $doc['image'];?>" alt="Image <?php echo $doc['title'];?>"/>
        </div>
    </article>
	<?php } ?>
    <div id="mainList">
    	<table>
        	<colgroup>
            	<col class="col1">
            	<col class="col2">
            	<col class="col3">
            	<col class="col4">
            	<col class="col5">
            </colgroup>
        	<tbody>
        	<?php
        		$nb = count($items);
        		for($i=0; $i<$nb; $i++){
        		$doc = $items[$i];
        		?>
        	<tr class="listItem cat<?php echo $doc['exp_cat'] ?>">
            	<td><?php echo ($i+1);?></td>
            	<td><?php echo isset($doc['when_txt']) ? $doc['when_txt'] : '--h--';?></td>
            	<td><?php echo $doc['exp_label'];?></td>
            	<td><?php echo $doc['title'];?></td>
            	<td><?php echo $doc['subtitle'];?></td>
            	<?php if($i < $nb - 1){?>
            <tr class="spacer"><td colspan="5"></td></tr>
            	<?php }?>
            </tr>
        	<?php } ?>
		</tbody></table>
        <div id="listButton">
	         <!-- <a class="listButtonPrint" href="index.php">Imprimer cette liste</a>
            <a class="listButtonShare" href="index.php">Partager cette liste</a> -->
            <a class="listButtonExp" href="mailto:propose@the-chic-list.com">Proposer une expérience</a>
        </div>
	</div>
</section>

<?php include('aside.php') ?>

<footer>
</footer>

</div>

<?php include('footer_scripts.php') ?>
<script src="js/main.js"></script>

</body></html>