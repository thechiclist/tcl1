<!DOCTYPE html>
<html>
<head>
<?php include('html_common_head.php') ?>
<link href="css/billet.css" rel="stylesheet" type="text/css">
<title>The Chic List - </title>
</head>
<body>

<div id="mainWrapper">

<?php include('header.php') ?>

<section>
	<article class="article cat<?php echo $doc_exp_cat ?>">
    	<div class="articleLeft">
       		<p class="articleSticker"><a href="#"><?php echo $doc['chic_clics'];?> chic clics</a></p>
       		<p class="articleStickerLong" style="display:none"><a href="#">Ce qu'en dit la Chic rédaction...</a></p>
       		<?php 
       			if(isset($docExp)){
       		?>
       		<p class="text1"><?php echo $docExp;?></p>
       		<?php }?>
            <p class="text2"><?php echo $doc['when_txt'];?></p>
            <p class="text3"><?php echo $doc['title'];?></p>
            <p class="text4"><?php echo $doc['subtitle'];?></p>
            <p class="question">Où ?</p>
            <p class="answer"><?php echo $doc['where_txt'];?></p>
            <p class="question">Quand ?</p>
            <p class="answer"><?php echo $doc['when_txt'];?></p>
            <!--
            <p class="question">Chic Book ?</p>
            <p class="answer">CB_Cuisine_Voyage_Maroc.</p>
             <div id="articleButton">
                <a class="buttonBook" href="#">Chic Book</a>
                <a class="buttonGeo" href="#">Géolocaliser</a>
                <a class="buttonShare" href="#">Partager</a>
                <a class="buttonAdd" href="#">Ajouter</a>
            </div> -->
        </div>
    	<div class="articleRight">
        	<img src="imgBase/<?php echo $doc['image'];?>" alt="Image principale">
        </div>
    	<div class="articleBottom cat10">
        	<?php echo $doc['content'];
        	if(isset($list)){?>
            <a class="back" href="list-<?php echo $list['id']; ?>.html" target="_self">Retour à la Chic List</a>
            <?php }?>
        </div>
    </article>
</section>

<?php include('aside.php') ?>

<footer>
</footer>

</div>

<?php include('footer_scripts.php') ?>


</body></html>