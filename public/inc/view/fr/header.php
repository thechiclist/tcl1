<header>	
	<div id="headerTop">
		<!-- 
		<div id="headerLogin"><a href="#"><p>CHIC TEAM : s'identifier</p></a></div>
        <div id="headerLogin_open">
        	<form method="post" action="#">
            	<label for="login">Identifiant Chic Team</label>
                <input type="text" id="login" name="login">
            	<label for="password">Mot de passe</label>
                <input type="password" id="password" name="password">
            </form>
            <a id="EE" href="#">J'ai oublié mon mot de passe</a>
            <a href="#">Créer un compte Chic Team</a>
        </div>
        <div id="headerLang"><a href="#">FR</a></div>
        <div id="headerLang_open">
        	<ul>
            	<li style="display: none;"><a href="#">FR</a></li>
            	<li><a href="#">EN</a></li>
            	<li><a href="#">DE</a></li>
            </ul>
        </div>
		 -->
    </div>
    <div id="headerSearch">
    	<form action="list.php" method="GET" id="searchForm">
	    	<div id="searchWhere">
	        	<p>Où?</p>
	            <img src="img/arrow_right_on_black.png" alt="arrow right">
	            <div class="scrollbar" style="display: none;"><div class="arrowUp" style="width: 11px; height: 11px; background-image: url(img/arrow_up_on_black.png);"></div><div class="line" style="width: 1px; height: 157px; margin: 5px 0px 5px 5px; background-color: rgb(204, 204, 204);"></div><div class="arrowDown" style="width: 11px; height: 11px; background-image: url(img/arrow_down_on_black.png);"></div></div>
	        	<div class="container">
	        		<?php $where_t = new CategoryTree(1, $lang);
	        			$where_t->ulClasses = array('menu','subMenu');
	        			$where_t->liClasses = array('toggleSubMenu');
	        			$where_t->printIt() ?>
	            </div>
	            <input name="where" type="hidden"/>
	        </div>
	    	<div id="searchWhen">
	        	<p>Quand?</p>
	            <img src="img/arrow_right_on_black.png" alt="arrow right">
	            <div class="scrollbar" style="display: none;"><div class="arrowUp" style="width: 11px; height: 11px; background-image: url(img/arrow_up_on_black.png);"></div><div class="line" style="width: 1px; height: 157px; margin: 5px 0px 5px 5px; background-color: rgb(204, 204, 204);"></div><div class="arrowDown" style="width: 11px; height: 11px; background-image: url(img/arrow_down_on_black.png);"></div></div>
	        	<div class="container">
	            	<ul class="menu">
	                	<li class="toggleSubMenu"><a href="#">Aujourd'hui</a>
	                    	<ul class="subMenu" style="display: none;">
	                        	<li><a href="#">Heure :</a></li>
	                        </ul>
	                    </li>
	                	<li class="toggleSubMenu"><a href="#">Demain</a>
	                    	<ul class="subMenu" style="display: none;">
	                        	<li><a href="#">Heure :</a></li>
	                        </ul>
	                    </li>
	                	<li class="toggleSubMenu"><a href="#">Un autre jour</a>
	                    	<ul class="subMenu" style="display: none;">
	                        	<li><a href="#">Jour :</a></li>
	                        	<li><a href="#">Heure :</a></li>
	                        </ul>
	                    </li>
	                </ul>
	            </div>
	            <input name="when" type="hidden"/>
	        </div>
	  	 	<div id="searchWhat">
	        	<p>Expérience?</p>
	            <img src="img/arrow_right_on_black.png" alt="arrow right">
	            <div class="scrollbar" style="display: none;"><div class="arrowUp" style="width: 11px; height: 11px; background-image: url(img/arrow_up_on_black.png);"></div><div class="line" style="width: 1px; height: 157px; margin: 5px 0px 5px 5px; background-color: rgb(204, 204, 204);"></div><div class="arrowDown" style="width: 11px; height: 11px; background-image: url(img/arrow_down_on_black.png);"></div></div>
	        	<div class="container">
	        		<?php $exp_t = new CategoryTree(2, $lang);
	        			$exp_t->ulClasses = array('menu','subMenu');
	        			$exp_t->liClasses = array('toggleSubMenu');
	        			$exp_t->printIt() ?>
	            </div>
	            <input name="exp" type="hidden"/>
	        </div>
	        <input class="searchButton" type="submit" value=""/>
	        <!--<input class="searchButton" type="submit"/>  <a href="#" class="searchButton" id="searchButton"></a> -->
    	</form>
    </div>
    <img id="logo" src="img/logo.png" alt="The Chic List logo">
    <h1 id="headerTitle"><span class="titleNumber">7 </span><em>Expériences à faire</em> pour sortir de l'ordinaire <strong><?php echo $listDate; ?></strong> à <strong><?php echo $listPlace; ?></strong>.</h1>
</header>