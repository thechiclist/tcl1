<aside>
	<div id="asideLeft">
    	<h2>découvrez</h2>
        <h1>La Chic vie</h1>
        <p>Vous en avez assez du traditionnel resto-ciné et disposez de peu de temps pour vous documenter sur ce qui sort des sentiers battus ?
The Chic List est là pour vous sortir de l'ordinaire ! Son équipe de rédaction vous propose tous les jours un chic menu de produits frais du jour cuisinés avec amour et présentés avec soin dans la ville où vous vous trouvez. 
Et puis, si aucune des proposition de ce menu ne vous convient pas, vous pourrez toujours rechercher une autre chic expérience dans les 9 chic catégories du chic annuaire : boire et manger, se cultiver s immanquables !</p>
    </div>
    <div id="asideRight">
    	<img src="img/image_aside.gif" alt="image The Chic vie">
        <div>
        	<p class="asideRigthText1">Que pour les enfants !</p>
        	<p class="asideRigthText2">Sur The Chic List, retrouvez toutes les activités culturelles, d'éveil ou de loisir proposés pour les enfants dans votre ville.</p>
        </div>
    </div>
</aside>