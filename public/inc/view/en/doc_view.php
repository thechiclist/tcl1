<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>The Chic List - </title>
<meta charset="utf-8">

<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Rosario"/>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Antic"/>
<link href="css/css" rel="stylesheet" type="text/css">
<link href="css/css(1)" rel="stylesheet" type="text/css">
<link href="css/main.css" rel="stylesheet" type="text/css">
<link href="css/billet.css" rel="stylesheet" type="text/css">
<link href="css/menus.css" rel="stylesheet" type="text/css">

<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" />
<link rel="icon" type="video/x-mng" href="img/favicon.mng" /> 

</head>
<body>

<div id="mainWrapper">

<header>	
	<div id="headerTop">
    	<div id="headerLogin"><a href="#"><p>CHIC TEAM : s'identifier</p></a></div>
        <div id="headerLogin_open">
        	<form method="post" action="#">
            	<label for="login">Identifiant Chic Team</label>
                <input type="text" id="login" name="login">
            	<label for="password">Mot de passe</label>
                <input type="password" id="password" name="password">
            </form>
            <a id="EE" href="#">J'ai oublié mon mot de passe</a>
            <a href="#">Créer un compte Chic Team</a>
        </div>
        <div id="headerLang"><a href="#">FR</a></div>
        <div id="headerLang_open">
        	<ul>
            	<li style="display: none;"><a href="#">FR</a></li>
            	<li><a href="#">EN</a></li>
            	<li><a href="#">DE</a></li>
            </ul>
        </div>
    </div>
    <div id="headerSearch">
    	<div id="searchWhere">
        	<p>Où?</p>
            <img src="img/arrow_right_on_black.png" alt="arrow right">
            <div class="scrollbar" style="display: none;"><div class="arrowUp" style="width: 11px; height: 11px; background-image: url(img/arrow_up_on_black.png);"></div><div class="line" style="width: 1px; height: 157px; margin: 5px 0px 5px 5px; background-color: rgb(204, 204, 204);"></div><div class="arrowDown" style="width: 11px; height: 11px; background-image: url(img/arrow_down_on_black.png);"></div></div>
        	<div class="container">
            	<ul class="menu">
                	<li class="toggleSubMenu"><a href="#">Baden-Baden (DE)</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="#">Balg</a></li>
                        	<li><a href="#">Ebersteinburg</a></li>
                        	<li><a href="#">Gaisbach</a></li>
                        	<li><a href="#">Haueneberstein</a></li>
                        	<li><a href="#">Lichtental</a></li>
                        	<li><a href="#">Neuweier</a></li>
                        	<li><a href="#">Oos</a></li>
                        	<li><a href="#">Sandweier</a></li>
                        	<li><a href="#">Steinbach</a></li>
                        	<li><a href="#">Varnhalt</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="#">Bâle (CH)</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="#">Grossbasel</a></li>
                        	<li><a href="#">Kleinbasel</a></li>
                        	<li><a href="#">Altstadt Grossbasel</a></li>
                        	<li><a href="#">Altstadt Kleinbasel</a></li>
                        	<li><a href="#">Am Ring</a></li>
                        	<li><a href="#">Bachletten</a></li>
                        	<li><a href="#">Breite</a></li>
                        	<li><a href="#">Bruderholz</a></li>
                        	<li><a href="#">Clara</a></li>
                        	<li><a href="#">Gotthelf</a></li>
                        	<li><a href="#">Gundeldingen</a></li>
                        	<li><a href="#">Hirzbrunnen</a></li>
                        	<li><a href="#">Iselin</a></li>
                        	<li><a href="#">Kleinhüningen</a></li>
                        	<li><a href="#">Klybeck</a></li>
                        	<li><a href="#">Matthäus</a></li>
                        	<li><a href="#">Neubad </a></li>
                        	<li><a href="#">Rosental</a></li>
                        	<li><a href="#">St. Alban</a></li>
                        	<li><a href="#">St. Johann</a></li>
                        	<li><a href="#">Vorstädte</a></li>
                        	<li><a href="#">Wettstein</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="#">Bordeaux</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="#">Capucins-Saint Michel</a></li>
                        	<li><a href="#">Caudéran-Barrière Judaïque</a></li>
                        	<li><a href="#">Grand Parc - Paul Doumergue</a></li>
                        	<li><a href="#">Hôtel De Ville - quinconce</a></li>
                        	<li><a href="#">La Bastide </a></li>
                        	<li><a href="#">Le Lac-Bacalan </a></li>
                        	<li><a href="#">Saint Bruno</a></li>
                        	<li><a href="#">Saint Jean-Belcier</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="#">Lilles</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="#">Centre</a></li>
                        	<li><a href="#">Hypercentre</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="#">Marseille</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="#">Centre</a></li>
                        	<li><a href="#">Hypercentre</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="#">Montpellier</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="#">Centre</a></li>
                        	<li><a href="#">Hypercentre</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="#">Mulhouse</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="#">Centre</a></li>
                        	<li><a href="#">Hypercentre</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="#">Rennes</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="#">Centre</a></li>
                        	<li><a href="#">Hypercentre</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="#">Strasbourg</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="#">Centre</a></li>
                        	<li><a href="#">Hypercentre</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="#">Toulouse</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="#">Centre</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    	<div id="searchWhen">
        	<p>Quand?</p>
            <img src="img/arrow_right_on_black.png" alt="arrow right">
            <div class="scrollbar" style="display: none;"><div class="arrowUp" style="width: 11px; height: 11px; background-image: url(img/arrow_up_on_black.png);"></div><div class="line" style="width: 1px; height: 157px; margin: 5px 0px 5px 5px; background-color: rgb(204, 204, 204);"></div><div class="arrowDown" style="width: 11px; height: 11px; background-image: url(img/arrow_down_on_black.png);"></div></div>
        	<div class="container">
            	<ul class="menu">
                	<li class="toggleSubMenu"><a href="#">Aujourd'hui</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="#">Heure :</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="#">Demain</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="#">Heure :</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="#">Un autre jour</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="#">Jour :</a></li>
                        	<li><a href="#">Heure :</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
  	 	<div id="searchWhat">
        	<p>Expérience?</p>
            <img src="img/arrow_right_on_black.png" alt="arrow right">
            <div class="scrollbar" style="display: none;"><div class="arrowUp" style="width: 11px; height: 11px; background-image: url(img/arrow_up_on_black.png);"></div><div class="line" style="width: 1px; height: 157px; margin: 5px 0px 5px 5px; background-color: rgb(204, 204, 204);"></div><div class="arrowDown" style="width: 11px; height: 11px; background-image: url(img/arrow_down_on_black.png);"></div></div>
        	<div class="container">
            	<ul class="menu">
                    <li class="toggleSubMenu"><a href="#">Boire et manger</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="#">repas léger</a></li>
                        	<li><a href="#">dégustation</a></li>
                        	<li><a href="#">vins fins et spiritueux</a></li>
                        	<li><a href="#">bonbons &amp; chocolats</a></li>
                        	<li><a href="#">pique-nique</a></li>
                        	<li><a href="#">traiteur, épicerie fine</a></li>
                        	<li><a href="#">souper raffiné</a></li>
                        	<li><a href="#">dîner aux chandelles</a></li>
                        	<li><a href="#">thé ou café ?</a></li>
                        	<li><a href="#">sur le pouce</a></li>
                        	<li><a href="#">apéros &amp; afterworks</a></li>
                        	<li><a href="#">petit-déjeuner, brunch</a></li>
                        	<li><a href="#">terrasse et plein air</a></li>
                        	<li><a href="#">fruits de mer</a></li>
                        	<li><a href="#">alsacien </a></li>
                        	<li><a href="#">entre amis</a></li>
                        	<li><a href="#">dîner spectacle</a></li>
                        	<li><a href="#">pâtisseries</a></li>
                        	<li><a href="#">pour les becs sucrés</a></li>
                        	<li><a href="#">au marché</a></li>
                        	<li><a href="#">un chef à la maison</a></li>
                        	<li><a href="#">livraison à domicile</a></li>
                        	<li><a href="#">la recette du jour</a></li>
                        	<li><a href="#">à emporter</a></li>
                        </ul>
                	</li>
                    <li class="toggleSubMenu"><a href="#">Dormir</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="#">chambre d'hôtes</a></li>
                        	<li><a href="#">au centre ville </a></li>
                        	<li><a href="#">charmant hôtel</a></li>
                        </ul>
                	</li>
                    <li class="toggleSubMenu"><a href="#">Être solidaire</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="#">être bénévole</a></li>
                        	<li><a href="#">donner, partager</a></li>
                        	<li><a href="#">voyager</a></li>
                        	<li><a href="#">s'engager</a></li>
                        	<li><a href="#">s'informer</a></li>
                        	<li><a href="#">la vie en vert</a></li>
                        </ul>
                	</li>
                    <li class="toggleSubMenu"><a href="#">Faire du shopping</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="#">chic &amp; cheap</a></li>
                        	<li><a href="#">meubles, objets et design</a></li>
                        	<li><a href="#">essentiels accessoires</a></li>
                        	<li><a href="#">chaussures et bagagerie</a></li>
                        	<li><a href="#">nature et sport</a></li>
                        	<li><a href="#">sport &amp; streetwear</a></li>
                        	<li><a href="#">belles plantes</a></li>
                        	<li><a href="#">dessous chics</a></li>
                        	<li><a href="#">chic &amp; décontracté</a></li>
                        	<li><a href="#">éternel féminin</a></li>
                        	<li><a href="#">dandys</a></li>
                        	<li><a href="#">jeux et jouets</a></li>
                        	<li><a href="#">parfums et bijoux</a></li>
                        	<li><a href="#">musique maestro !</a></li>
                        	<li><a href="#">l'heure des créateurs</a></li>
                        	<li><a href="#">idées de cadeau</a></li>
                        	<li><a href="#">livres</a></li>
                        </ul>
                	</li>
                    <li class="toggleSubMenu"><a href="#">Prendre soin de soi </a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="#">activités physiques douces</a></li>
                        	<li><a href="#">sport</a></li>
                        	<li><a href="#">soins du corps</a></li>
                        	<li><a href="#">spa et thermes</a></li>
                        	<li><a href="#">se faire coiffer, maquiller</a></li>
                        	<li><a href="#">se faire masser</a></li>
                        	<li><a href="#">manucure</a></li>
                        	<li><a href="#">faire du sport</a></li>
                        	<li><a href="#">se relaxer</a></li>
                        </ul>
                	</li>
                    <li class="toggleSubMenu"><a href="#">Rester chez soi</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="#">télé, vidéos &amp; VOD</a></li>
                        	<li><a href="#">bons bouquins</a></li>
                        	<li><a href="#">à la radio…</a></li>
                        	<li><a href="#">rien à cuisiner</a></li>
                        	<li><a href="#">cultiver son jardin ou son balcon</a></li>
                        	<li><a href="#">bricoler</a></li>
                        	<li><a href="#">le bon bouquin</a></li>
                        	<li><a href="#">acheter sur Internet</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="#">S’occuper des enfants</a>
                        <ul class="subMenu" style="display: none;">
                        	<li><a href="#">contes</a></li>
                        	<li><a href="#">les habiller</a></li>
                        	<li><a href="#">les activité du mercredis</a></li>
                        	<li><a href="#">la ferme !</a></li>
                        	<li><a href="#">plein air</a></li>
                        	<li><a href="#">artistes en herbe</a></li>
                        	<li><a href="#">spectacles</a></li>
                        	<li><a href="#">apprendre en s'amusant</a></li>
                        	<li><a href="#">cirque</a></li>
                        	<li><a href="#">activités sportives</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="#">S’organiser</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="#">coup d'aspiro</a></li>
                        	<li><a href="#">louer</a></li>
                        	<li><a href="#">services publics</a></li>
                        	<li><a href="#">se faire livrer ses courses</a></li>
                        	<li><a href="#">baby-sitting et garde d'enfants</a></li>
                        	<li><a href="#">petits et grands travaux</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="#">Sortir</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="#">au théâtre ce soir</a></li>
                        	<li><a href="#">concerts et festivals</a></li>
                        	<li><a href="#">art lyrique et opéra</a></li>
                        	<li><a href="#">danse</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="#">Voir et savoir</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="#">architecture et patrimoine</a></li>
                        	<li><a href="#">conférences</a></li>
                        	<li><a href="#">expositions temporaires</a></li>
                        	<li><a href="#">apprendre à…</a></li>
                        	<li><a href="#">musées</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <a href="#" class="searchButton"></a>
    </div>
    <img id="logo" src="img/logo.png" alt="The Chic List logo">
    <h1 id="headerTitle"><span class="titleNumber">7 </span><em>Expériences à faire</em> pour sortir de l'ordinaire <strong><?php echo $listDate; ?></strong> à <strong><?php echo $listPlace; ?></strong>.</h1>
</header>

<section>
	<article class="article">
    	<div class="articleLeft">
       		<p class="articleSticker"><a href="#"><?php echo $doc['chic_clics'];?> chic clics</a></p>
       		<p class="articleStickerLong" style="display:none"><a href="#">Ce qu'en dit la Chic rédaction...</a></p>
       		<?php 
       			if(isset($docExp)){
       		?>
       		<p class="text1"><?php echo $docExp;?></p>
       		<?php }?>
            <p class="text2"><?php echo $doc['when_txt'];?></p>
            <p class="text3"><?php echo $doc['title'];?></p>
            <p class="text4"><?php echo $doc['subtitle'];?></p>
            <p class="question">Où ?</p>
            <p class="answer cat10"><?php echo $doc['where_txt'];?></p>
            <p class="question">Quand ?</p>
            <p class="answer cat10"><?php echo $doc['when_txt'];?></p>
            <!--
            <p class="question">Chic Book ?</p>
            <p class="answer cat10">CB_Cuisine_Voyage_Maroc.</p>
             <div id="articleButton">
                <a class="buttonBook" href="#">Chic Book</a>
                <a class="buttonGeo" href="#">Géolocaliser</a>
                <a class="buttonShare" href="#">Partager</a>
                <a class="buttonAdd" href="#">Ajouter</a>
            </div> -->
        </div>
    	<div class="articleRight">
        	<img src="imgBase/<?php echo $doc['image'];?>" alt="Image principale">
        </div>
    	<div class="articleBottom cat10">
        	<?php echo $doc['content'];
        	if(isset($list)){?>
            <a class="back" href="list-<?php echo $list['id']; ?>.html" target="_self">Retour à la Chic List</a>
            <?php }?>
        </div>
    </article>
</section>


<aside>
	<div id="asideLeft">
    	<h2>découvrez</h2>
        <h1>La Chic vie</h1>
        <p>Vous en avez assez du traditionnel resto-ciné et disposez de peu de temps pour vous documenter sur ce qui sort des sentiers battus ?
The Chic List est là pour vous sortir de l'ordinaire ! Son équipe de rédaction vous propose tous les jours un chic menu de produits frais du jour cuisinés avec amour et présentés avec soin dans la ville où vous vous trouvez. 
Et puis, si aucune des proposition de ce menu ne vous convient pas, vous pourrez toujours rechercher une autre chic expérience dans les 9 chic catégories du chic annuaire : boire et manger, se cultiver s immanquables !</p>
    </div>
    <div id="asideRight">
    	<img src="img/image_aside.gif" alt="image The Chic vie">
        <div>
        	<p class="asideRigthText1">Que pour les enfants !</p>
        	<p class="asideRigthText2">Sur The Chic List, retrouvez toutes les activités culturelles, d'éveil ou de loisir proposés pour les enfants dans votre ville.</p>
        </div>
    </div>
</aside>

<footer>
</footer>

</div>

<script src="js/jquery-1.9.1.min.js"></script>
<script src="js/jquery-ui-1.10.1.custom.min.js"></script>
<script src="js/menus.js"></script>

</body></html>