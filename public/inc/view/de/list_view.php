<!DOCTYPE html>
<!-- saved from url=(0078)http://www.multimedia-engineering.fr/test/327_Marie_ChicList/TheChicList.html# -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>The Chic List - </title>
<meta charset="utf-8">

<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<link href="css/css.css" rel="stylesheet" type="text/css">
<link href="css/css1.css" rel="stylesheet" type="text/css">
<link href="css/main.css" rel="stylesheet" type="text/css">
<link href="css/menus.css" rel="stylesheet" type="text/css">

<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" />
<link rel="icon" type="video/x-mng" href="img/favicon.mng" /> 

</head>
<body>

<div id="mainWrapper">

<header>	
	<div id="headerTop">
    	<div id="headerLogin"><a href="index.php"><p>CHIC TEAM : s'identifier</p></a></div>
        <div id="headerLogin_open">
        	<form method="post" action="index.php">
            	<label for="login">Identifiant Chic Team</label>
                <input type="text" id="login" name="login">
            	<label for="password">Mot de passe</label>
                <input type="password" id="password" name="password">
            </form>
            <a id="EE" href="index.php">J'ai oublié mon mot de passe</a>
            <a href="index.php">Créer un compte Chic Team</a>
        </div>
        <div id="headerLang"><a href="index.php">FR</a></div>
        <div id="headerLang_open">
        	<ul>
            	<li style="display: none;"><a href="index.php">FR</a></li>
            	<li><a href="index.php">EN</a></li>
            	<li><a href="index.php">DE</a></li>
            </ul>
        </div>
    </div>
    <div id="headerSearch">
    	<div id="searchWhere" style="height: 200px;">
        	<p style="color: rgb(73, 178, 206);">Où?</p>
            <img src="img/arrow_right_on_black.png" alt="arrow right" style="display: none;">
            <div class="scrollbar" style="display: none;"><div class="arrowUp" style="width: 11px; height: 11px; background-image: url(img/arrow_up_on_black.png);"></div><div class="line" style="width: 1px; height: 157px; margin: 5px 0px 5px 5px; background-color: rgb(204, 204, 204);"></div><div class="arrowDown" style="width: 11px; height: 11px; background-image: url(img/arrow_down_on_black.png);"></div></div>
        	<div class="container">
            	<ul class="menu">
                	<li class="toggleSubMenu"><a href="index.php">Baden-Baden (DE)</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="index.php">Balg</a></li>
                        	<li><a href="index.php">Ebersteinburg</a></li>
                        	<li><a href="index.php">Gaisbach</a></li>
                        	<li><a href="index.php">Haueneberstein</a></li>
                        	<li><a href="index.php">Lichtental</a></li>
                        	<li><a href="index.php">Neuweier</a></li>
                        	<li><a href="index.php">Oos</a></li>
                        	<li><a href="index.php">Sandweier</a></li>
                        	<li><a href="index.php">Steinbach</a></li>
                        	<li><a href="index.php">Varnhalt</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="index.php">Bâle (CH)</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="index.php">Grossbasel</a></li>
                        	<li><a href="index.php">Kleinbasel</a></li>
                        	<li><a href="index.php">Altstadt Grossbasel</a></li>
                        	<li><a href="index.php">Altstadt Kleinbasel</a></li>
                        	<li><a href="index.php">Am Ring</a></li>
                        	<li><a href="index.php">Bachletten</a></li>
                        	<li><a href="index.php">Breite</a></li>
                        	<li><a href="index.php">Bruderholz</a></li>
                        	<li><a href="index.php">Clara</a></li>
                        	<li><a href="index.php">Gotthelf</a></li>
                        	<li><a href="index.php">Gundeldingen</a></li>
                        	<li><a href="index.php">Hirzbrunnen</a></li>
                        	<li><a href="index.php">Iselin</a></li>
                        	<li><a href="index.php">Kleinhüningen</a></li>
                        	<li><a href="index.php">Klybeck</a></li>
                        	<li><a href="index.php">Matthäus</a></li>
                        	<li><a href="index.php">Neubad </a></li>
                        	<li><a href="index.php">Rosental</a></li>
                        	<li><a href="index.php">St. Alban</a></li>
                        	<li><a href="index.php">St. Johann</a></li>
                        	<li><a href="index.php">Vorstädte</a></li>
                        	<li><a href="index.php">Wettstein</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="index.php">Bordeaux</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="index.php">Capucins-Saint Michel</a></li>
                        	<li><a href="index.php">Caudéran-Barrière Judaïque</a></li>
                        	<li><a href="index.php">Grand Parc - Paul Doumergue</a></li>
                        	<li><a href="index.php">Hôtel De Ville - quinconce</a></li>
                        	<li><a href="index.php">La Bastide </a></li>
                        	<li><a href="index.php">Le Lac-Bacalan </a></li>
                        	<li><a href="index.php">Saint Bruno</a></li>
                        	<li><a href="index.php">Saint Jean-Belcier</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="index.php">Lilles</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="index.php">Centre</a></li>
                        	<li><a href="index.php">Hypercentre</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="index.php">Marseille</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="index.php">Centre</a></li>
                        	<li><a href="index.php">Hypercentre</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="index.php">Montpellier</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="index.php">Centre</a></li>
                        	<li><a href="index.php">Hypercentre</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="index.php">Mulhouse</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="index.php">Centre</a></li>
                        	<li><a href="index.php">Hypercentre</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="index.php">Rennes</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="index.php">Centre</a></li>
                        	<li><a href="index.php">Hypercentre</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="index.php">Strasbourg</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="index.php">Centre</a></li>
                        	<li><a href="index.php">Hypercentre</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="index.php">Toulouse</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="index.php">Centre</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    	<div id="searchWhen" style="height: 20px;">
        	<p style="color: rgb(128, 128, 128);">Quand?</p>
            <img src="img/arrow_right_on_black.png" alt="arrow right" style="display: block;">
            <div class="scrollbar" style="display: none;"><div class="arrowUp" style="width: 11px; height: 11px; background-image: url(img/arrow_up_on_black.png);"></div><div class="line" style="width: 1px; height: 157px; margin: 5px 0px 5px 5px; background-color: rgb(204, 204, 204);"></div><div class="arrowDown" style="width: 11px; height: 11px; background-image: url(img/arrow_down_on_black.png);"></div></div>
        	<div class="container">
            	<ul class="menu">
                	<li class="toggleSubMenu"><a href="index.php">Aujourd'hui</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="index.php">Heure :</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="index.php">Demain</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="index.php">Heure :</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="index.php">Un autre jour</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="index.php">Jour :</a></li>
                        	<li><a href="index.php">Heure :</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
  	 	<div id="searchWhat" style="height: 200px;">
        	<p style="color: rgb(73, 178, 206);">Expérience?</p>
            <img src="img/arrow_right_on_black.png" alt="arrow right" style="display: none;">
            <div class="scrollbar" style="display: none;"><div class="arrowUp" style="width: 11px; height: 11px; background-image: url(img/arrow_up_on_black.png);"></div><div class="line" style="width: 1px; height: 157px; margin: 5px 0px 5px 5px; background-color: rgb(204, 204, 204);"></div><div class="arrowDown" style="width: 11px; height: 11px; background-image: url(img/arrow_down_on_black.png);"></div></div>
        	<div class="container">
            	<ul class="menu">
                    <li class="toggleSubMenu"><a href="index.php">Boire et manger</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="index.php">repas léger</a></li>
                        	<li><a href="index.php">dégustation</a></li>
                        	<li><a href="index.php">vins fins et spiritueux</a></li>
                        	<li><a href="index.php">bonbons &amp; chocolats</a></li>
                        	<li><a href="index.php">pique-nique</a></li>
                        	<li><a href="index.php">traiteur, épicerie fine</a></li>
                        	<li><a href="index.php">souper raffiné</a></li>
                        	<li><a href="index.php">dîner aux chandelles</a></li>
                        	<li><a href="index.php">thé ou café ?</a></li>
                        	<li><a href="index.php">sur le pouce</a></li>
                        	<li><a href="index.php">apéros &amp; afterworks</a></li>
                        	<li><a href="index.php">petit-déjeuner, brunch</a></li>
                        	<li><a href="index.php">terrasse et plein air</a></li>
                        	<li><a href="index.php">fruits de mer</a></li>
                        	<li><a href="index.php">alsacien </a></li>
                        	<li><a href="index.php">entre amis</a></li>
                        	<li><a href="index.php">dîner spectacle</a></li>
                        	<li><a href="index.php">pâtisseries</a></li>
                        	<li><a href="index.php">pour les becs sucrés</a></li>
                        	<li><a href="index.php">au marché</a></li>
                        	<li><a href="index.php">un chef à la maison</a></li>
                        	<li><a href="index.php">livraison à domicile</a></li>
                        	<li><a href="index.php">la recette du jour</a></li>
                        	<li><a href="index.php">à emporter</a></li>
                        </ul>
                	</li>
                    <li class="toggleSubMenu"><a href="index.php">Dormir</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="index.php">chambre d'hôtes</a></li>
                        	<li><a href="index.php">au centre ville </a></li>
                        	<li><a href="index.php">charmant hôtel</a></li>
                        </ul>
                	</li>
                    <li class="toggleSubMenu"><a href="index.php">Être solidaire</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="index.php">être bénévole</a></li>
                        	<li><a href="index.php">donner, partager</a></li>
                        	<li><a href="index.php">voyager</a></li>
                        	<li><a href="index.php">s'engager</a></li>
                        	<li><a href="index.php">s'informer</a></li>
                        	<li><a href="index.php">la vie en vert</a></li>
                        </ul>
                	</li>
                    <li class="toggleSubMenu"><a href="index.php">Faire du shopping</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="index.php">chic &amp; cheap</a></li>
                        	<li><a href="index.php">meubles, objets et design</a></li>
                        	<li><a href="index.php">essentiels accessoires</a></li>
                        	<li><a href="index.php">chaussures et bagagerie</a></li>
                        	<li><a href="index.php">nature et sport</a></li>
                        	<li><a href="index.php">sport &amp; streetwear</a></li>
                        	<li><a href="index.php">belles plantes</a></li>
                        	<li><a href="index.php">dessous chics</a></li>
                        	<li><a href="index.php">chic &amp; décontracté</a></li>
                        	<li><a href="index.php">éternel féminin</a></li>
                        	<li><a href="index.php">dandys</a></li>
                        	<li><a href="index.php">jeux et jouets</a></li>
                        	<li><a href="index.php">parfums et bijoux</a></li>
                        	<li><a href="index.php">musique maestro !</a></li>
                        	<li><a href="index.php">l'heure des créateurs</a></li>
                        	<li><a href="index.php">idées de cadeau</a></li>
                        	<li><a href="index.php">livres</a></li>
                        </ul>
                	</li>
                    <li class="toggleSubMenu"><a href="index.php">Prendre soin de soi </a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="index.php">activités physiques douces</a></li>
                        	<li><a href="index.php">sport</a></li>
                        	<li><a href="index.php">soins du corps</a></li>
                        	<li><a href="index.php">spa et thermes</a></li>
                        	<li><a href="index.php">se faire coiffer, maquiller</a></li>
                        	<li><a href="index.php">se faire masser</a></li>
                        	<li><a href="index.php">manucure</a></li>
                        	<li><a href="index.php">faire du sport</a></li>
                        	<li><a href="index.php">se relaxer</a></li>
                        </ul>
                	</li>
                    <li class="toggleSubMenu"><a href="index.php">Rester chez soi</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="index.php">télé, vidéos &amp; VOD</a></li>
                        	<li><a href="index.php">bons bouquins</a></li>
                        	<li><a href="index.php">à la radio…</a></li>
                        	<li><a href="index.php">rien à cuisiner</a></li>
                        	<li><a href="index.php">cultiver son jardin ou son balcon</a></li>
                        	<li><a href="index.php">bricoler</a></li>
                        	<li><a href="index.php">le bon bouquin</a></li>
                        	<li><a href="index.php">acheter sur Internet</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="index.php">S’occuper des enfants</a>
                        <ul class="subMenu" style="display: none;">
                        	<li><a href="index.php">contes</a></li>
                        	<li><a href="index.php">les habiller</a></li>
                        	<li><a href="index.php">les activité du mercredis</a></li>
                        	<li><a href="index.php">la ferme !</a></li>
                        	<li><a href="index.php">plein air</a></li>
                        	<li><a href="index.php">artistes en herbe</a></li>
                        	<li><a href="index.php">spectacles</a></li>
                        	<li><a href="index.php">apprendre en s'amusant</a></li>
                        	<li><a href="index.php">cirque</a></li>
                        	<li><a href="index.php">activités sportives</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="index.php">S’organiser</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="index.php">coup d'aspiro</a></li>
                        	<li><a href="index.php">louer</a></li>
                        	<li><a href="index.php">services publics</a></li>
                        	<li><a href="index.php">se faire livrer ses courses</a></li>
                        	<li><a href="index.php">baby-sitting et garde d'enfants</a></li>
                        	<li><a href="index.php">petits et grands travaux</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="index.php">Sortir</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="index.php">au théâtre ce soir</a></li>
                        	<li><a href="index.php">concerts et festivals</a></li>
                        	<li><a href="index.php">art lyrique et opéra</a></li>
                        	<li><a href="index.php">danse</a></li>
                        </ul>
                    </li>
                	<li class="toggleSubMenu"><a href="index.php">Voir et savoir</a>
                    	<ul class="subMenu" style="display: none;">
                        	<li><a href="index.php">architecture et patrimoine</a></li>
                        	<li><a href="index.php">conférences</a></li>
                        	<li><a href="index.php">expositions temporaires</a></li>
                        	<li><a href="index.php">apprendre à…</a></li>
                        	<li><a href="index.php">musées</a></li>
                        </ul>
                    </li>
               </ul>
            </div>
        </div>
        <a href="index.php" class="searchButton"></a>
    </div>
    <img id="logo" src="img/logo.png" alt="The Chic List logo">
    <h1 id="headerTitle"><span class="titleNumber">7 </span><em>Expériences à faire</em> pour sortir de l'ordinaire <strong>aujourd'hui</strong> à <strong>strasbourg</strong>.</h1>
</header>

<section>
	<article class="article" style="position: absolute; left: -980px;">
    	<div class="articleLeft">
      		<p class="articleSticker"><a href="index.php">124 chic clics</a></p>
       		<p class="text1">Prendre soin de soi</p>
            <p class="text2">quand on veut</p>
            <p class="text3">Journée Day Spa Bio</p>
            <p class="text4">La Clairière</p>
            <div class="articleBillet"><a href="doc.php?key=1" target="_self">Chic Billet</a></div>
            <div class="articleBook"><a href="index.php">Chic Book</a></div>
        </div>
    	<div class="articleRight">
        	<img src="img/image4.jpg" alt="image 4">
        </div>
    </article>
	<article class="article" style="position: absolute; left: -980px;">
    	<div class="articleLeft">
      		<p class="articleSticker"><a href="index.php">124 chic clics</a></p>
       		<p class="text1">Voir &amp; apprendre</p>
            <p class="text2">10h15</p>
            <p class="text3">Cours de cuisine : voyage au Maroc</p>
            <p class="text4">Atelier des chefs</p>
            <div class="articleBillet"><a href="doc.php?key=2" target="_self">Chic Billet</a></div>
            <div class="articleBook"><a href="index.php">Chic Book</a></div>
        </div>
    	<div class="articleRight">
        	<img src="img/image2.jpg" alt="image 2">
        </div>
    </article>
	<article class="article" style="position: absolute; left: 0px;">
    	<div class="articleLeft">
      		<p class="articleSticker"><a href="index.php">124 chic clics</a></p>
       		<p class="text1">Voir &amp; apprendre</p>
            <p class="text2">14h00</p>
            <p class="text3">Visite guidée gratuite de Strasbourg</p>
            <p class="text4">Local Trotter</p>
            <div class="articleBillet"><a href="doc.php?key=3" target="_self">Chic Billet</a></div>
            <div class="articleBook"><a href="index.php">Chic Book</a></div>
        </div>
    	<div class="articleRight">
        	<img src="img/image3.jpg" alt="image 3">
        </div>
    </article>
	<article class="article" style="position: absolute; left: -980px;">
    	<div class="articleLeft">
       		<p class="articleSticker"><a href="index.php">124 chic clics</a></p>
       		<p class="text1">Faire du shopping</p>
            <p class="text2">quand on veut</p>
            <p class="text3">Beau linge</p>
            <p class="text4">chez Sarah Verguet</p>
            <div class="articleBillet"><a href="doc.php?key=4" target="_self">Chic Billet</a></div>
            <div class="articleBook"><a href="index.php">Chic Book</a></div>
        </div>
    	<div class="articleRight">
        	<img src="img/image1.jpg" alt="image 1">
        </div>
    </article>
	<article class="article" style="position: absolute; left: -980px;">
    	<div class="articleLeft">
      		<p class="articleSticker"><a href="index.php">124 chic clics</a></p>
       		<p class="text1">Boire &amp; manger</p>
            <p class="text2">19h00</p>
            <p class="text3">Le Restaurant Savoyard</p>
            <p class="text4">Etoile des neiges</p>
            <div class="articleBillet"><a href="doc.php?key=5" target="_self">Chic Billet</a></div>
            <div class="articleBook"><a href="index.php">Chic Book</a></div>
       </div>
    	<div class="articleRight">
        	<img src="img/image5.jpg" alt="image 5">
        </div>
    </article>
	<article class="article" style="position: absolute; left: -980px;">
    	<div class="articleLeft">
      		<p class="articleSticker"><a href="index.php">124 chic clics</a></p>
       		<p class="text1">Sortir</p>
            <p class="text2">20h15</p>
            <p class="text3">Tu honoreras ta mère et ta mère</p>
            <p class="text4">Cinéma Star</p>
            <div class="articleBillet"><a href="doc.php?key=6" target="_self">Chic Billet</a></div>
            <div class="articleBook"><a href="index.php">Chic Book</a></div>
        </div>
    	<div class="articleRight">
        	<img src="img/image6.jpg" alt="image 6">
        </div>
    </article>
	<article class="article" style="position: absolute; left: -980px;">
    	<div class="articleLeft">
      		<p class="articleSticker"><a href="index.php">124 chic clics</a></p>
      		<p class="text1">Voir &amp; apprendre</p>
            <p class="text2">20h20</p>
            <p class="text3">Dr. CAC : Apprenez l'économie sans souci</p>
            <p class="text4">France 5</p>
            <div class="articleBillet"><a href="doc.php?key=7" target="_self">Chic Billet</a></div>
            <div class="articleBook"><a href="index.php">Chic Book</a></div>
        </div>
    	<div class="articleRight">
        	<img src="img/image7.jpg" alt="image 7">
        </div>
    </article>
    <div id="mainList">
    	<table>
        	<colgroup>
            	<col class="col1">
            	<col class="col2">
            	<col class="col3">
            	<col class="col4">
            	<col class="col5">
            </colgroup>
        	<tbody><tr class="listItem cat5" style="background-color: rgb(239, 239, 239);">
            	<td style="background-color: rgb(207, 206, 206);">1</td>
            	<td>--h--</td>
            	<td>Prendre soin de soi</td>
            	<td>Journée Day Spa Bio</td>
            	<td>La Clairière</td>
            </tr>
            <tr class="spacer"><td colspan="5"></td></tr>
        	<tr class="listItem cat10" style="background-color: rgb(239, 239, 239);">
            	<td style="background-color: rgb(207, 206, 206);">2</td>
            	<td>10h15</td>
            	<td>Voir &amp; apprendre</td>
            	<td>Cours de cuisine : voyage au Maroc</td>
            	<td>Atelier des chefs</td>
            </tr>
            <tr class="spacer"><td colspan="5"></td></tr>
        	<tr class="listItem cat10 listItem_activated" style="background-color: rgb(56, 74, 166);">
            	<td style="background-color: rgb(56, 74, 166);">3</td>
            	<td>14h00</td>
            	<td>Voir &amp; apprendre</td>
            	<td>Visite guidée gratuite de Strasbourg</td>
            	<td>Local Trotter</td>
            </tr>
             <tr class="spacer"><td colspan="5"></td></tr>
        	<tr class="listItem cat4" style="background-color: rgb(239, 239, 239);">
            	<td style="background-color: rgb(207, 206, 206);">4</td>
            	<td>--h--</td>
            	<td>Faire du shopping</td>
            	<td>Beau Linge</td>
            	<td>Sarah Verguet</td>
            </tr>
            <tr class="spacer"><td colspan="5"></td></tr>
        	<tr class="listItem cat1" style="background-color: rgb(239, 239, 239);">
            	<td style="background-color: rgb(207, 206, 206);">5</td>
            	<td>19h00</td>
            	<td>Boire &amp; manger</td>
            	<td>Le Restaurant Savoyard</td>
            	<td>Etoile des neiges</td>
            </tr>
            <tr class="spacer"><td colspan="5"></td></tr>
        	<tr class="listItem cat9" style="background-color: rgb(239, 239, 239);">
            	<td style="background-color: rgb(207, 206, 206);">6</td>
            	<td>20h15</td>
            	<td>Sortir</td>
            	<td>Tu honoreras ta mère et ta mère</td>
            	<td>Cinéma Star</td>
            </tr>
            <tr class="spacer"><td colspan="5"></td></tr>
        	<tr class="listItem cat10" style="background-color: rgb(239, 239, 239);">
            	<td style="background-color: rgb(207, 206, 206);">7</td>
            	<td>20h20</td>
            	<td>Voir &amp; apprendre</td>
            	<td>Dr. CAC : Apprenez l'économie sans souci</td>
            	<td>France 5</td>
            </tr>
		</tbody></table>
        <div id="listButton">
            <a class="listButtonPrint" href="index.php">Imprimer cette liste</a>
            <a class="listButtonShare" href="index.php">Partager cette liste</a>
            <a class="listButtonExp" href="index.php">Proposer une expérience</a>
        </div>
	</div>
</section>


<aside>
	<div id="asideLeft">
    	<h2>découvrez</h2>
        <h1>La Chic vie</h1>
        <p>Vous en avez assez du traditionnel resto-ciné et disposez de peu de temps pour vous documenter sur ce qui sort des sentiers battus ?
The Chic List est là pour vous sortir de l'ordinaire ! Son équipe de rédaction vous propose tous les jours un chic menu de produits frais du jour cuisinés avec amour et présentés avec soin dans la ville où vous vous trouvez. 
Et puis, si aucune des proposition de ce menu ne vous convient pas, vous pourrez toujours rechercher une autre chic expérience dans les 9 chic catégories du chic annuaire : boire et manger, se cultiver s immanquables !</p>
    </div>
    <div id="asideRight">
    	<img src="img/image_aside.gif" alt="image The Chic vie">
        <div>
        	<p class="asideRigthText1">Que pour les enfants !</p>
        	<p class="asideRigthText2">Sur The Chic List, retrouvez toutes les activités culturelles, d'éveil ou de loisir proposés pour les enfants dans votre ville.</p>
        </div>
    </div>
</aside>

<footer>
</footer>

</div>

<script src="js/jquery-1.9.1.min.js"></script>
<script src="js/jquery-ui-1.10.1.custom.min.js"></script>
<script src="js/main.js"></script>
<script src="js/menus.js"></script>


</body></html>