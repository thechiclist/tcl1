<?php
include('inc/prolog.php');
$lang = DEFAULT_LANG;
$client = new PhpWebClient();
$client->start();

if(isset($_REQUEST['list'])){
	$listId = addslashes($_REQUEST['list']);
}

if(isset($listId)){
	$list = DAO::getList($client, $listId);
}

// Get list based on request
$noFallback = false;
if(!isset($list) && (isset($_REQUEST['when']) || isset($_REQUEST['exp']))){
	$where = isset($_REQUEST['where']) ? addslashes($_REQUEST['where']) : DEFAULT_PLACE; 
	$q = 'select *
		from chic_list
		where status=\'online\' and place_cat='.$where;
	if(isset($_REQUEST['exp'])){
		$exp = addslashes($_REQUEST['exp']);
		$q .= ' and exp_cat='.$exp;
		$noFallback = true; // Show explicitly that no results have been found.
	}else if(isset($_REQUEST['when'])){
		$when = addslashes($_REQUEST['when']);
		$q .= ' and date_filter=\''.$when.'\'';
		$noFallback = true; // Show explicitly that no results have been found.
	}else{
		$q .= ' and date_filter=\''.date('Y-m-d').'\''; // Get the list of the day
	} 
	$res = $client->execQuery($q);
		
	if($res){
		$list = Db::fetchAssoc($res);
	}
}

// Get the latest list of the default place.
if(!$noFallback){
	$q = 'select *
	from chic_list
	where status=\'online\' and place_cat='.DEFAULT_PLACE.' order by created desc';
	$res = $client->execQuery($q);
	
	if($res){
		$list = Db::fetchAssoc($res);
	}
}

if(!isset($list)){
	header('Location: 404.php');
	exit();
}else{

	// Get list elements
	$res = $client->execQuery(
			'select dm.*,d.*,c.label_'.$lang.' as exp_label
			from doc_master dm
			inner join doc d on d.master=dm.id
			inner join chic_list_doc_master l on l.doc_master=dm.id
			left join category c on dm.exp_cat=c.id
			where d.status=\'online\' and lang=\''.$lang.'\' and l.chic_list='.$list['id'].' order by l.rank');

	$items = array();
	if($res){
		while($row = Db::fetchAssoc($res)){
			array_push($items, $row);
		}
	}

	// Get list filters
	$exp_cat = DAO::getCategory($client, $list, 'exp_cat', $lang);

	$listDate = DateUtils::getDateStrObj($list, 'date_filter');
	$listPlace = DAO::getCategory($client, $list, 'place_cat', $lang);
}

include('inc/view/'.$lang.'/list_view.php');
?>