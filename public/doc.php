<?php
include('inc/prolog.php');
$lang = DEFAULT_LANG;

if(isset($_REQUEST['docTarget'])){
	$docTarget = addslashes($_REQUEST['docTarget']);
}

if(isset($docTarget)){
	$client = new PhpWebClient();
	$client->start();
	
	$res = $client->execQuery(
				'select dm.*,d.*
				from doc_master dm
				inner join doc d on d.master=dm.id
				where d.status=\'online\' and lang=\''.$lang.'\' and dm.target=\''.$docTarget.'\' order by d.id');

	if($res){
		$doc = Db::fetchAssoc($res);
		$docExp = DAO::getCategory($client, $doc, 'exp_cat', $lang);
		$doc_exp_cat = isset($doc['exp_cat']) ? $doc['exp_cat'] : 0;
	}
	
	if(isset($_REQUEST['list'])){
		$list = DAO::getList($client, $_REQUEST['list']);
		$listDate = DateUtils::getDateStrObj($list, 'date_cat');
		$listPlace = DAO::getCategory($client, $list, 'place_cat', $lang);
	}
}

if(!isset($doc)){
	header('Location: 404.php');
	exit();
}

include('inc/view/fr/doc_view.php');
?>