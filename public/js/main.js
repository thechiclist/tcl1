

$(function(){

// init elements
var DELAY = 4000;

var articleArr = $('.article');		// array of <article> (left panel + main image)
var listItemArr = $('.listItem');	// array of <tr> for the main list
var linkArr = $('.article .articleBillet>a');	// array of <a> to billets pages

var currArticle = undefined;
var lastArticle = undefined;
var isAnimating = false;

// add links to itemlist
listItemArr.on('click', function(evt)
{
	doClick(evt);
});

// double click
listItemArr.on('dblclick', function(evt)
{
	doDoubleClick(evt);
});

// hide all articles
$('.article').css({'position':'absolute', 'left':'980px'});


// start from a billet page
var itemNumber = getUrlVars()['item'];
if (itemNumber != undefined)
{
	var item = parseInt(itemNumber, 10);
	displayArticle(item, false);
	startAnimation();
}
else
{
	// start animation with item 0
	displayArticle(0, false);
	startAnimation();
}




// functions --------------------------------------------------------------------------------

function getUrlVars() // var id = getUrlVars()["id"];
{
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value)
	{
        vars[key] = value;
    });
    return vars;
}

function displayArticle(index, animate)
{
	if (index == lastArticle) return;
	
	// move out article if one is selected
	if (lastArticle != undefined) {
		articleArr.eq(lastArticle).stop().animate({'left':'-980px'}, 1000, 'easeInOutExpo');

		// deselect item list
		//listItemArr[lastArticle].setAttribute('class', 'listItem');
	}
	if (animate)
	{
		articleArr.eq(index).css('left', '980px');
		articleArr.eq(index).stop().animate({'left':'0px'}, 1000, 'easeInOutExpo', function()
		{
			if (isAnimating) startAnimation();
		});
	}
	else
	{
		$('.article').eq(index).css('left', '0px');
	}
	
	// change class to activated in list
	deselectAllItemList();
	selectItemList(index);
	
	currArticle = index;
	lastArticle = index;
}

function deselectAllItemList()
{
	//listItemArr.css('background-color', '#efefef');	// reset back the color to light grey
	//listItemArr.find('td:first-child').css('background-color', '#CFCECE');	// colorize as well 1st cell of row
	listItemArr.removeClass('listItem_activated');
	listItemArr.each(function(){
		var $t = $(this);
		$t.attr('class', $t.attr('class').replace(/(cat\d+)active/,'$1'));
	});
}

function selectItemList(index)
{
	var $selItem = listItemArr.eq(index);
	$selItem.addClass('listItem_activated');
	$selItem.attr('class', $selItem.attr('class').replace(/(cat\d+)/,'$1active'));
}

 
function startAnimation()
{
	isAnimating = true;
	currArticle++;
	if (currArticle > 6) currArticle = 0;
	setTimeout(function()
	{
		displayArticle(currArticle, true);
	}, DELAY);
}

function stopAnimation()
{
	isAnimating = false;
}

function doClick(evt)
{
	// find which item list has been clicked
	for (var i=0, len=listItemArr.length; i<len; i++)
	{
		if ($(evt.target).parent().is(listItemArr.eq(i)))
		{
			var index = i;
			break;
		}
	}
	stopAnimation();
	displayArticle(index, true);
}

function doDoubleClick(evt)
{
	// find which item list has been clicked
	for (var i=0, len=listItemArr.length; i<len; i++)
	{
		if ($(evt.target).parent().is(listItemArr.eq(i)))
		{
			var index = i;
			break;
		}
	}
	stopAnimation();
	
	// open link
	var url = linkArr.eq(index).attr('href');
	window.open(url, '_self');
}


});