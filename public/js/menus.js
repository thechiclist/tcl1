

$(function(){


// language drop down menu --------------------------------------------------------------------------------------------
$('#headerLang a').on('click', function(){	
	// show arrow
	if ($('#headerLang_open').css('display') == 'none')
	{
	}
	else
	{
	}
	// open/close
	$('#headerLang_open').slideToggle('fast');
	$('#headerLogin_open').slideUp('fast');
});
	
// select a language
$('#headerLang_open li').on('click', function(index){
	
	// set all li elements visible
	$('#headerLang_open li').show();
	$(this).hide();
	$('#headerLang_open').hide();
	
	// replace the text in the top menu
	var chosenLang = $(this).text();
	$('#headerLang a').text(chosenLang);
	console.log('chosen language: '+chosenLang);
});

// hide FR by default
$('#headerLang_open li:first').hide();


// login drop down menu --------------------------------------------------------------------------------------------
$('#headerLogin a').on('click', function(){
	// open/close
	$('#headerLogin_open').slideToggle('fast');
	$('#headerLang_open').slideUp('fast');
});



// search menus ------------------------------------------------------------------------------------------------------
var MAX_CONTAINER_HEIGHT = 180;

// hide scrollbar
$('#headerSearch div.scrollbar').hide();

// create all scrollbars
createAllScrollbars();

// close menus when search button is cliked
$('#headerSearch .searchButton').on('click', function(){
	var box = $('#searchForm>div');
	box.animate({height:20}, 'fast');
	box.find('img').show();
	box.find('p').css('color', '#808080');
		
	// scrollbar
	$('#headerSearch').find('.scrollbar').hide();
});

$('#searchForm>div').on('click', function(){
	var currHeight = $(this).css('height');
	
	if (currHeight == '20px')
	{
		// open
		$(this).stop().animate({height:200}, 'fast');
		$(this).find('img').hide();
		$(this).find('p').css('color', '#49B2CE');
		
		// scrollbar
		displayScrollbar($(this).find('.container'), $(this).find('.scrollbar'));
	}
	else
	{
		// close
		$(this).stop().animate({height:20}, 'fast');
		$(this).find('img').show();
		$(this).find('p').css('color', '#808080');
		
		// scrollbar
		$(this).find('.scrollbar').hide();
	}
});

// On cache les sous-menus :
$(".menu ul.subMenu").hide();
// On sélectionne tous les items de liste portant la classe "toggleSubMenu"

// On modifie l'évènement "click" sur les liens dans les items de liste
// qui portent la classe "toggleSubMenu" :
$(".menu li.toggleSubMenu > a").click( function (){		
	var container = $(this).parent().parent();
	var scrollbar = container.parent().parent().find('.scrollbar');
	// Si le sous-menu était déjà ouvert, on le referme :
	if ($(this).next("ul.subMenu:visible").length != 0)
	{	
		$(this).next("ul.subMenu").parent().removeClass("open");
		$(this).next("ul.subMenu").slideUp("normal", function()
		{
			// scrollbar
			displayScrollbar(container, scrollbar);
		});
	}
	// Si le sous-menu est caché, on ferme les autres et on l'affiche :
	else
	{
		$(".menu ul.subMenu").parent().removeClass("open");
		$(".menu ul.subMenu").slideUp("normal", function()
		{
			// scrollbar
			displayScrollbar(container, scrollbar);
		});
		
		$(this).parent().addClass("open")
		$(this).next("ul.subMenu").slideDown("normal", function()
		{
			// scrollbar
			displayScrollbar(container, scrollbar);
		});
	}
	
	// On empêche le navigateur de suivre le lien :
	return false;
}); 

// on modifie l'event click sur le subMenu   
$(".menu .subMenu a").click( function (){
	//si le subMenu est deja checked; on le un-check
	if ($(this).parent().hasClass('checked'))
	{
		$(this).parent().removeClass('checked');
	}
	else
	{
		$(this).parent().addClass('checked');
	}
	
	// On empêche le navigateur de suivre le lien :
	return false;
}); 

// private function -----------------------------------------------------------------------------------------

function displayScrollbar(container, jScrollbar){
	var containerHeight = parseInt(container.css('height'), 10);	

	if (containerHeight > MAX_CONTAINER_HEIGHT)
	{
		// display scrollbar
		jScrollbar.show();
	}
	else
	{
		jScrollbar.hide();
	}
}

function createAllScrollbars(){
	$("<div class='arrowUp'></div><div class='line'></div><div class='arrowDown'></div>").appendTo($('#headerSearch div.scrollbar'));

	// add graphics
	$('#headerSearch div.scrollbar .arrowUp').css({'width':'11px', 'height':'11px', 'background-image':'url("images/arrow_up_on_black.png")'});
	$('#headerSearch div.scrollbar .line').css({'width':'1px', 'height':'157px', 'margin':'5px 0px 5px 5px', 'background-color':'#ccc'});
	$('#headerSearch div.scrollbar .arrowDown').css({'width':'11px', 'height':'11px', 'background-image':'url("images/arrow_down_on_black.png")'});
	
/*	
	// add rolovers
	$('#headerSearch div.scrollbar .arrowUp').on('mouseover', function()
	{
		var container = $(this).parent().parent().find('.container');
		console.log('up over');
	});
	$('#headerSearch div.scrollbar .arrowUp').on('mouseout', function()
	{
		console.log('up out');
	});
	$('#headerSearch div.scrollbar .arrowDown').on('mouseover', function()
	{
		console.log('down over');
	});
	$('#headerSearch div.scrollbar .arrowDown').on('mouseout', function()
	{
		console.log('down out');
	});
*/
}

});
