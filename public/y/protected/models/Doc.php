<?php

/**
 * This is the model class for table "doc".
 *
 * The followings are the available columns in table 'doc':
 * @property string $id
 * @property string $master
 * @property string $lang
 * @property string $status
 * @property string $created
 * @property string $title
 * @property string $subtitle
 * @property string $image
 * @property string $when
 * @property string $content
 */
class Doc extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Doc the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'doc';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('master, created, title, subtitle, image, content', 'required'),
			array('master', 'length', 'max'=>10),
			array('lang', 'length', 'max'=>2),
			array('status', 'length', 'max'=>8),
			array('title, subtitle, image', 'length', 'max'=>200),
			array('when', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, master, lang, status, created, title, subtitle, image, when, content', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
            'master'=>array(self::BELONGS_TO, 'DocMaster', 'master'),
            'categories'=>array(self::MANY_MANY, 'Category',
                'category_doc(doc, category)'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'master' => 'Master',
			'lang' => 'Lang',
			'status' => 'Status',
			'created' => 'Created',
			'title' => 'Title',
			'subtitle' => 'Subtitle',
			'image' => 'Image',
			'when' => 'When',
			'content' => 'Content',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('master',$this->master,true);
		$criteria->compare('lang',$this->lang,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('subtitle',$this->subtitle,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('when',$this->when,true);
		$criteria->compare('content',$this->content,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}