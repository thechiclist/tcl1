<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property string $id
 * @property string $label
 * @property string $email
 * @property string $password
 * @property string $created
 * @property string $last_connected
 * @property string $status
 * @property integer $connection_serie
 * @property integer $connection_token
 */
class User extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('label, email, password, created, last_connected, status', 'required'),
			array('connection_serie, connection_token', 'numerical', 'integerOnly'=>true),
			array('label', 'length', 'max'=>100),
			array('email', 'length', 'max'=>255),
			array('password', 'length', 'max'=>30),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, label, email, password, created, last_connected, status, connection_serie, connection_token', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'label' => 'Label',
			'email' => 'Email',
			'password' => 'Password',
			'created' => 'Created',
			'last_connected' => 'Last Connected',
			'status' => 'Status',
			'connection_serie' => 'Connection Serie',
			'connection_token' => 'Connection Token',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('label',$this->label,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('last_connected',$this->last_connected,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('connection_serie',$this->connection_serie);
		$criteria->compare('connection_token',$this->connection_token);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}