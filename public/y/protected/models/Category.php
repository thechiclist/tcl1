<?php

/**
 * This is the model class for table "category".
 *
 * The followings are the available columns in table 'category':
 * @property string $id
 * @property string $status
 * @property string $parent_cat
 * @property integer $rank
 * @property string $label_fr
 * @property string $label_en
 * @property string $label_de
 * @property string $label_it
 */
class Category extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Category the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('rank', 'required'),
			array('rank', 'numerical', 'integerOnly'=>true),
			array('status', 'length', 'max'=>8),
			array('parent_cat', 'length', 'max'=>10),
			array('label_fr, label_en, label_de, label_it', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, status, parent_cat, rank, label_fr, label_en, label_de, label_it', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'status' => 'Status',
			'parent_cat' => 'Parent Cat',
			'rank' => 'Rank',
			'label_fr' => 'Label Fr',
			'label_en' => 'Label En',
			'label_de' => 'Label De',
			'label_it' => 'Label It',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('parent_cat',$this->parent_cat,true);
		$criteria->compare('rank',$this->rank);
		$criteria->compare('label_fr',$this->label_fr,true);
		$criteria->compare('label_en',$this->label_en,true);
		$criteria->compare('label_de',$this->label_de,true);
		$criteria->compare('label_it',$this->label_it,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}