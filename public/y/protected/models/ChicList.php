<?php

/**
 * This is the model class for table "chic_list".
 *
 * The followings are the available columns in table 'chic_list':
 * @property string $id
 * @property string $status
 * @property string $created
 * @property string $place_cat
 * @property string $exp_cat
 * @property string $date_filter
 */
class ChicList extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ChicList the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'chic_list';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('created', 'required'),
			array('status', 'length', 'max'=>8),
			array('place_cat, exp_cat, date_filter', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, status, created, place_cat, exp_cat, date_filter', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'status' => 'Status',
			'created' => 'Created',
			'place_cat' => 'Place Cat',
			'exp_cat' => 'Exp Cat',
			'date_filter' => 'Date Filter',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('place_cat',$this->place_cat,true);
		$criteria->compare('exp_cat',$this->exp_cat,true);
		$criteria->compare('date_filter',$this->date_filter,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}