<?php
/* @var $this EditorController */
/* @var $model DocMaster */

$this->breadcrumbs=array(
	'Doc Masters'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List DocMaster', 'url'=>array('index')),
	array('label'=>'Create DocMaster', 'url'=>array('create')),
	array('label'=>'Update DocMaster', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DocMaster', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DocMaster', 'url'=>array('admin')),
);
?>

<h1>View DocMaster #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'target',
		'type',
		'author',
	),
)); ?>
