<?php
/* @var $this EditorController */
/* @var $model DocMaster */

$this->breadcrumbs=array(
	'Doc Masters'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DocMaster', 'url'=>array('index')),
	array('label'=>'Create DocMaster', 'url'=>array('create')),
	array('label'=>'View DocMaster', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DocMaster', 'url'=>array('admin')),
);
?>

<h1>Update DocMaster <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>