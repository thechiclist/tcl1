<?php
/* @var $this EditorController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Doc Masters',
);

$this->menu=array(
	array('label'=>'Create DocMaster', 'url'=>array('create')),
	array('label'=>'Manage DocMaster', 'url'=>array('admin')),
);
?>

<h1>Doc Masters</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
