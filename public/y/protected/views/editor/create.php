<?php
/* @var $this EditorController */
/* @var $model DocMaster */

$this->breadcrumbs=array(
	'Doc Masters'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DocMaster', 'url'=>array('index')),
	array('label'=>'Manage DocMaster', 'url'=>array('admin')),
);
?>

<h1>Create DocMaster</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>