<?php
/* @var $this CategoryController */
/* @var $model Category */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'category-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>8,'maxlength'=>8)); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'parent_cat'); ?>
		<?php echo $form->textField($model,'parent_cat',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'parent_cat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'label_fr'); ?>
		<?php echo $form->textField($model,'label_fr',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'label_fr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'label_en'); ?>
		<?php echo $form->textField($model,'label_en',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'label_en'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'label_de'); ?>
		<?php echo $form->textField($model,'label_de',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'label_de'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'label_it'); ?>
		<?php echo $form->textField($model,'label_it',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'label_it'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->