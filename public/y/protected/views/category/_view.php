<?php
/* @var $this CategoryController */
/* @var $data Category */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parent_cat')); ?>:</b>
	<?php echo CHtml::encode($data->parent_cat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('label_fr')); ?>:</b>
	<?php echo CHtml::encode($data->label_fr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('label_en')); ?>:</b>
	<?php echo CHtml::encode($data->label_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('label_de')); ?>:</b>
	<?php echo CHtml::encode($data->label_de); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('label_it')); ?>:</b>
	<?php echo CHtml::encode($data->label_it); ?>
	<br />


</div>