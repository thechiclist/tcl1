<?php
/* @var $this ChicListController */
/* @var $model ChicList */

$this->breadcrumbs=array(
	'Chic Lists'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ChicList', 'url'=>array('index')),
	array('label'=>'Create ChicList', 'url'=>array('create')),
	array('label'=>'Update ChicList', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ChicList', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ChicList', 'url'=>array('admin')),
);
?>

<h1>View ChicList #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'status',
		'created',
		'place_cat',
		'exp_cat',
		'date_cat',
	),
)); ?>
