<?php
/* @var $this ChicListController */
/* @var $model ChicList */

$this->breadcrumbs=array(
	'Chic Lists'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ChicList', 'url'=>array('index')),
	array('label'=>'Create ChicList', 'url'=>array('create')),
	array('label'=>'View ChicList', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ChicList', 'url'=>array('admin')),
);
?>

<h1>Update ChicList <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>