<?php
/* @var $this ChicListController */
/* @var $data ChicList */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('place_cat')); ?>:</b>
	<?php echo CHtml::encode($data->place_cat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('exp_cat')); ?>:</b>
	<?php echo CHtml::encode($data->exp_cat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_cat')); ?>:</b>
	<?php echo CHtml::encode($data->date_cat); ?>
	<br />


</div>