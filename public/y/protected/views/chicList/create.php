<?php
/* @var $this ChicListController */
/* @var $model ChicList */

$this->breadcrumbs=array(
	'Chic Lists'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ChicList', 'url'=>array('index')),
	array('label'=>'Manage ChicList', 'url'=>array('admin')),
);
?>

<h1>Create ChicList</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>