<?php
/* @var $this ChicListController */
/* @var $model ChicList */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'chic-list-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>8,'maxlength'=>8)); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
		<?php echo $form->error($model,'created'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'place_cat'); ?>
		<?php echo $form->textField($model,'place_cat',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'place_cat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'exp_cat'); ?>
		<?php echo $form->textField($model,'exp_cat',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'exp_cat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_cat'); ?>
		<?php echo $form->textField($model,'date_cat',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'date_cat'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->