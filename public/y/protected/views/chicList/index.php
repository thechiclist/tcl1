<?php
/* @var $this ChicListController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Chic Lists',
);

$this->menu=array(
	array('label'=>'Create ChicList', 'url'=>array('create')),
	array('label'=>'Manage ChicList', 'url'=>array('admin')),
);
?>

<h1>Chic Lists</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
