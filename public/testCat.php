<html>
<head>
<title>Test catégorie</title>
<meta charset="utf-8">
</head>
<body>
<?php include('inc/prolog.php');
$lang = DEFAULT_LANG;

$client = new PhpWebClient();
$client->start();

$exp_t = new CategoryTree(2, $lang);
$exp_t->ulClasses = array('menu','submenu');
$exp_t->liClasses = array('toggleSubMenu');
$exp_t->printIt() ?>
</body>
</html>