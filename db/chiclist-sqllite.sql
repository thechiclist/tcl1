# SQLiteManager Dump
# Version: 1.2.4
# http://www.sqlitemanager.org/
# 
# Serveur: localhost
# Généré le: Monday 25th 2013f February 2013 05:15 pm
# SQLite Version: 3.7.7.1
# PHP Version: 5.3.13
# Base de données: chiclist.sqlite3
# --------------------------------------------------------

#
# Structure de la table: category
#
CREATE TABLE 'category' ( 'id' INTEGER NOT NULL PRIMARY KEY, 'status' varchar(8) NOT NULL DEFAULT 'draft' , 'parent_cat' int(10) DEFAULT NULL , 'rank' tinyint(3) NOT NULL , 'label_fr' varchar(255) DEFAULT NULL , 'label_en' varchar(255) DEFAULT NULL , 'label_de' varchar(255) DEFAULT NULL , 'label_it' varchar(255) DEFAULT NULL );
CREATE INDEX category_parent_cat ON 'category' ('parent_cat');

#
# Contenu de la table: category
#
INSERT INTO 'category' VALUES ('1', 'online', NULL, '0', 'OU', NULL, NULL, NULL);
INSERT INTO 'category' VALUES ('2', 'online', NULL, '0', 'EXP', NULL, NULL, NULL);
INSERT INTO 'category' VALUES ('3', 'online', '2', '1', 'Boire et manger', 'Food and drink', 'Essen und Trinken', 'Cibo e bevande');
INSERT INTO 'category' VALUES ('4', 'online', '3', '2', 'repas léger', 'light meal', 'leichte Mahlzeit', 'pasto leggero');
INSERT INTO 'category' VALUES ('5', 'online', '3', '3', 'dégustation', 'tasting', 'Weinprobe', 'degustazione');
INSERT INTO 'category' VALUES ('6', 'online', '3', '4', 'vins fins et spiritueux', 'wines and spirits', 'Weine und Spirituosen', 'vini e liquori');
INSERT INTO 'category' VALUES ('7', 'online', '3', '5', 'bonbons &amp; chocolats', 'sweets & chocolates', 'Süßigkeiten und Schokolade', 'caramelle e cioccolatini');
INSERT INTO 'category' VALUES ('8', 'online', '3', '6', 'pique-nique', 'picnic', 'Picknick', 'picnic');
INSERT INTO 'category' VALUES ('9', 'online', '3', '7', 'traiteur, épicerie fine', 'catering, delicatessen', 'Catering, Delikatessen', 'catering, salumeria');
INSERT INTO 'category' VALUES ('10', 'online', '3', '8', 'souper raffiné', 'refined dinner', 'raffiniertes Dinner', 'cena raffinata');
INSERT INTO 'category' VALUES ('11', 'online', '3', '9', 'dîner aux chandelles', 'candlelight dinner', 'Candlelight-Dinner', 'a lume di candela cena');
INSERT INTO 'category' VALUES ('12', 'online', '3', '10', 'thé ou café ?', 'Tea or coffee?', 'Tee oder Kaffee?', 'Tè o caffè?');
INSERT INTO 'category' VALUES ('13', 'online', '3', '11', 'sur le pouce', 'on the go', 'auf dem Sprung', 'in movimento');
INSERT INTO 'category' VALUES ('14', 'online', '3', '12', 'apéros &amp; afterworks', 'appetizers & Afterworks', 'Vorspeisen & Afterworks', 'antipasti e Afterworks');
INSERT INTO 'category' VALUES ('15', 'online', '3', '13', 'petit-déjeuner, brunch', 'breakfast, brunch', 'Frühstück, Brunch', 'colazione, il brunch');
INSERT INTO 'category' VALUES ('16', 'online', '3', '14', 'terrasse et plein air', 'terrace and outdoor', 'Terrasse und Outdoor', 'terrazza e spazi esterni');
INSERT INTO 'category' VALUES ('17', 'online', '3', '15', 'fruits de mer', 'seafood', 'Meeresfrüchte', 'frutti di mare');
INSERT INTO 'category' VALUES ('18', 'online', '3', '16', 'alsacien', 'Alsatian', 'Elsässer', 'alsaziano');
INSERT INTO 'category' VALUES ('19', 'online', '3', '17', 'entre amis', 'friends', 'Freunden', 'amici');
INSERT INTO 'category' VALUES ('20', 'online', '3', '18', 'dîner spectacle', 'dinner show', 'Dinner-Show', 'cena spettacolo');
INSERT INTO 'category' VALUES ('21', 'online', '3', '19', 'pâtisseries', 'pastries', 'Gebäck', 'pasticceria');
INSERT INTO 'category' VALUES ('22', 'online', '3', '20', 'pour les becs sucrés', 'for the sweet tooth', 'für den süßen Zahn', 'per i più golosi');
INSERT INTO 'category' VALUES ('23', 'online', '3', '21', 'au marché', 'market', 'Markt', 'mercato');
INSERT INTO 'category' VALUES ('24', 'online', '3', '22', 'un chef à la maison', 'a leader at home', 'führend zu Hause', 'un leader in casa');
INSERT INTO 'category' VALUES ('25', 'online', '3', '23', 'livraison à domicile', 'home delivery', 'Hauszustellung', 'consegna a domicilio');
INSERT INTO 'category' VALUES ('26', 'online', '3', '24', 'la recette du jour', 'Recipe of the day', 'Rezept des Tages', 'Ricetta del giorno');
INSERT INTO 'category' VALUES ('27', 'online', '3', '25', 'à emporter', 'Takeaway', 'Takeaway', 'da asporto');
INSERT INTO 'category' VALUES ('28', 'online', '2', '26', 'Dormir', 'sleep', 'schlafen', 'dormire');
INSERT INTO 'category' VALUES ('29', 'online', '28', '27', 'chambre d''hôtes', 'guest', 'Gast', 'ospite');
INSERT INTO 'category' VALUES ('30', 'online', '28', '28', 'au centre ville', 'downtown', 'Innenstadt', 'centro');
INSERT INTO 'category' VALUES ('31', 'online', '28', '29', 'charmant hôtel', 'charming', 'charmant', 'affascinante');
INSERT INTO 'category' VALUES ('32', 'online', '2', '30', 'Être solidaire', 'be secured', 'gesichert werden', 'essere assicurato');
INSERT INTO 'category' VALUES ('33', 'online', '32', '31', 'être bénévole', 'be a volunteer', 'sein ein Freiwilliger', 'essere un volontario');
INSERT INTO 'category' VALUES ('34', 'online', '32', '32', 'donner, partager', 'give, share', 'geben, Aktien', 'dare, condividere');
INSERT INTO 'category' VALUES ('35', 'online', '32', '33', 'voyager', 'travel', 'reisen', 'viaggiare');
INSERT INTO 'category' VALUES ('36', 'online', '32', '34', 's''engager', 'engage', 'engagieren', 'impegnarsi');
INSERT INTO 'category' VALUES ('37', 'online', '32', '35', 's''informer', 'learn', 'lernen', 'imparare');
INSERT INTO 'category' VALUES ('38', 'online', '32', '36', 'la vie en vert', 'green living', 'grüne Wohnzimmer', 'vivere verde');
INSERT INTO 'category' VALUES ('39', 'online', '2', '37', 'Faire du shopping', 'Go shopping', 'Einkaufen gehen', 'Andare a fare shopping');
INSERT INTO 'category' VALUES ('40', 'online', '39', '38', 'chic &amp; cheap', 'chic and cheap', 'chic und günstig', 'chic and cheap');
INSERT INTO 'category' VALUES ('41', 'online', '39', '39', 'meubles, objets et design', 'furniture, and design', 'Möbel und Design', 'mobili, e il design');
INSERT INTO 'category' VALUES ('42', 'online', '39', '40', 'essentiels accessoires', 'essential accessories', 'notwendiges Zubehör', 'accessori indispensabili');
INSERT INTO 'category' VALUES ('43', 'online', '39', '41', 'chaussures et bagagerie', 'shoes and luggage', 'Schuhe und Gepäck', 'calzature e articoli da viaggio');
INSERT INTO 'category' VALUES ('44', 'online', '39', '42', 'nature et sport', 'nature and sport', 'Natur und Sport', 'natura e sport');
INSERT INTO 'category' VALUES ('45', 'online', '39', '43', 'sport &amp; streetwear', 'sports & streetwear', 'sports & Streetwear', 'sport & streetwear');
INSERT INTO 'category' VALUES ('46', 'online', '39', '44', 'belles plantes', 'beautiful plants', 'schöne Pflanzen', 'belle piante');
INSERT INTO 'category' VALUES ('47', 'online', '39', '45', 'dessous chics', 'dessous chics', 'dessous chics', 'dessous chics');
INSERT INTO 'category' VALUES ('48', 'online', '39', '46', 'chic &amp; décontracté', 'chic & casual', 'chic & casual', 'chic e casual');
INSERT INTO 'category' VALUES ('49', 'online', '39', '47', 'éternel féminin', 'eternal feminine', 'ewig-Weiblichen', 'eterno femminino');
INSERT INTO 'category' VALUES ('50', 'online', '39', '48', 'dandys', 'dandies', 'Dandys', 'dandy');
INSERT INTO 'category' VALUES ('51', 'online', '39', '49', 'jeux et jouets', 'games and toys', 'Spiele und Spielzeug', 'giochi e giocattoli');
INSERT INTO 'category' VALUES ('52', 'online', '39', '50', 'parfums et bijoux', 'perfume and jewelry', 'Parfüm und Schmuck', 'profumi e gioielli');
INSERT INTO 'category' VALUES ('53', 'online', '39', '51', 'musique maestro !', 'music maestro!', 'Musik maestro!', 'musica maestro!');
INSERT INTO 'category' VALUES ('54', 'online', '39', '52', 'l''heure des créateurs', 'creative time', 'kreative Zeit', 'momento creativo');
INSERT INTO 'category' VALUES ('55', 'online', '39', '53', 'idées de cadeau', 'gift ideas', 'Geschenkideen', 'idee regalo');
INSERT INTO 'category' VALUES ('56', 'online', '39', '54', 'livres', 'books', 'Bücher', 'libri');
INSERT INTO 'category' VALUES ('57', 'online', '2', '55', 'Prendre soin de soi', 'Taking care of yourself', 'Die Pflege von sich selbst', 'Prendersi cura di se stessi');
INSERT INTO 'category' VALUES ('58', 'online', '57', '56', 'activités physiques douces', 'mild physical activity', 'milde körperliche Aktivität', 'lieve attività fisica');
INSERT INTO 'category' VALUES ('59', 'online', '57', '57', 'sport', 'sport', 'Sport', 'sport');
INSERT INTO 'category' VALUES ('60', 'online', '57', '58', 'soins du corps', 'body care', 'Körperpflege', 'cura del corpo');
INSERT INTO 'category' VALUES ('61', 'online', '57', '59', 'spa et thermes', 'spa and spa', 'Spa und Wellness', 'spa e centro benessere');
INSERT INTO 'category' VALUES ('62', 'online', '57', '60', 'se faire coiffer, maquiller', 'their hair done, makeup', 'ihr Haar getan, Make-up', 'i loro capelli fatti, trucco');
INSERT INTO 'category' VALUES ('63', 'online', '57', '61', 'se faire masser', 'get a massage', 'eine Massage', 'ottenere un massaggio');
INSERT INTO 'category' VALUES ('64', 'online', '57', '62', 'manucure', 'manicure', 'Maniküre', 'manicure');
INSERT INTO 'category' VALUES ('65', 'online', '57', '63', 'faire du sport', 'make sport', 'machen Sport', 'fare sport');
INSERT INTO 'category' VALUES ('66', 'online', '57', '64', 'se relaxer', 'relax', 'entspannen', 'relax');
INSERT INTO 'category' VALUES ('67', 'online', '2', '65', 'Rester chez soi', 'Stay at home', 'Bleiben Sie zu Hause', 'Stai a casa');
INSERT INTO 'category' VALUES ('68', 'online', '67', '66', 'télé, vidéos &amp; VOD', 'TV, VOD & videos', 'TV, VOD und Videos', 'TV, VOD & video');
INSERT INTO 'category' VALUES ('69', 'online', '67', '67', 'bons bouquins', 'good books', 'gute Bücher', 'buoni libri');
INSERT INTO 'category' VALUES ('70', 'online', '67', '68', 'à la radio…', 'on the radio ...', 'auf der radio ...', 'alla radio ...');
INSERT INTO 'category' VALUES ('71', 'online', '67', '69', 'rien à cuisiner', 'no cooking', 'kein Kochen', 'non cucina');
INSERT INTO 'category' VALUES ('72', 'online', '67', '70', 'cultiver son jardin ou son balcon', 'cultivate his garden or balcony', 'kultivieren seinem Garten oder Balkon', 'coltivare il suo giardino o balcone');
INSERT INTO 'category' VALUES ('73', 'online', '67', '71', 'bricoler', 'tinker', 'basteln', 'rattoppare');
INSERT INTO 'category' VALUES ('74', 'online', '67', '72', 'le bon bouquin', 'the good book', 'das gute Buch', 'il buon libro');
INSERT INTO 'category' VALUES ('75', 'online', '67', '73', 'acheter sur Internet', 'buy on Internet', 'kaufen im Internet', 'acquistare su Internet');
INSERT INTO 'category' VALUES ('76', 'online', '2', '74', 'S’occuper des enfants', 'Caring for children', 'Betreuung von Kindern', 'Prendersi cura di bambini');
INSERT INTO 'category' VALUES ('77', 'online', '76', '75', 'contes', 'tales', 'Tales', 'Tales');
INSERT INTO 'category' VALUES ('78', 'online', '76', '76', 'les habiller', 'dress up', 'verkleiden', 'vestire');
INSERT INTO 'category' VALUES ('79', 'online', '76', '77', 'les activité du mercredis', 'the activity of Wednesdays', 'die Aktivität des Mittwochs', 'l''attività di mercoledì');
INSERT INTO 'category' VALUES ('80', 'online', '76', '78', 'la ferme !', 'shut up!', 'shut up!', 'stai zitto!');
INSERT INTO 'category' VALUES ('81', 'online', '76', '79', 'plein air', 'outdoor', 'im Freien', 'all''aperto');
INSERT INTO 'category' VALUES ('82', 'online', '76', '80', 'artistes en herbe', 'budding artists', 'angehende Künstler', 'artisti in erba');
INSERT INTO 'category' VALUES ('83', 'online', '76', '81', 'spectacles', 'sights', 'Sehenswürdigkeiten', 'Attrazioni');
INSERT INTO 'category' VALUES ('84', 'online', '76', '82', 'apprendre en s''amusant', 'learning fun', 'Lernen Spaß', 'divertente l''apprendimento');
INSERT INTO 'category' VALUES ('85', 'online', '76', '83', 'cirque', 'circus', 'Zirkus', 'circo');
INSERT INTO 'category' VALUES ('86', 'online', '76', '84', 'activités sportives', 'sports', 'Sport', 'dello sport');
INSERT INTO 'category' VALUES ('87', 'online', '2', '85', 'S’organiser', 'organize', 'organisieren', 'organizzare');
INSERT INTO 'category' VALUES ('88', 'online', '87', '86', 'coup d''aspiro', 'coup d''aspiro', 'coup d''Aspiro', 'colpo di ASPIRO');
INSERT INTO 'category' VALUES ('89', 'online', '87', '87', 'louer', 'rent', 'mieten', 'affitto');
INSERT INTO 'category' VALUES ('90', 'online', '87', '88', 'services publics', 'utilities', 'Dienstprogramme', 'programmi di utilità');
INSERT INTO 'category' VALUES ('91', 'online', '87', '89', 'se faire livrer ses courses', 'be delivering his races', 'ausliefern seine Rennen', 'consegnerà le sue gare');
INSERT INTO 'category' VALUES ('92', 'online', '87', '90', 'baby-sitting et garde d''enfants', 'babysitting and childcare', 'Babysitting und Kinderbetreuung', 'baby-sitter e assistenza all''infanzia');
INSERT INTO 'category' VALUES ('93', 'online', '87', '91', 'petits et grands travaux', 'large and small works', 'große und kleine Werke', 'opere di grandi e piccole');
INSERT INTO 'category' VALUES ('94', 'online', '2', '92', 'Sortir', 'exit', 'verlassen', 'uscita');
INSERT INTO 'category' VALUES ('95', 'online', '94', '93', 'au théâtre ce soir', 'theater tonight', 'Theater heute', 'teatro stasera');
INSERT INTO 'category' VALUES ('96', 'online', '94', '94', 'concerts et festivals', 'concerts and festivals', 'Konzerte und Festivals', 'concerti e festival');
INSERT INTO 'category' VALUES ('97', 'online', '94', '95', 'art lyrique et opéra', 'opera and opera', 'Oper und Oper', 'opera e opera');
INSERT INTO 'category' VALUES ('98', 'online', '94', '96', 'danse', 'dance', 'tanzen', 'ballare');
INSERT INTO 'category' VALUES ('99', 'online', '2', '97', 'Voir et savoir', 'Look and learn', 'Schauen und lernen', 'Guarda e impara');
INSERT INTO 'category' VALUES ('100', 'online', '99', '98', 'architecture et patrimoine', 'Heritage and architecture', 'Heritage und Architektur', 'Heritage e architettura');
INSERT INTO 'category' VALUES ('101', 'online', '99', '99', 'conférences', 'conferences', 'Konferenzen', 'conferenze');
INSERT INTO 'category' VALUES ('102', 'online', '99', '100', 'expositions temporaires', 'exhibitions', 'Ausstellungen', 'mostre');
INSERT INTO 'category' VALUES ('103', 'online', '99', '101', 'apprendre à...', 'learn how to ...', 'lernen, wie man ...', 'imparare a ...');
INSERT INTO 'category' VALUES ('104', 'online', '99', '102', 'musées', 'museums', 'Museen', 'musei');
INSERT INTO 'category' VALUES ('105', 'online', '1', '1', 'Baden-Baden (DE)', 'Baden-Baden (DE)', 'Baden-Baden', 'Baden-Baden (DE)');
INSERT INTO 'category' VALUES ('106', 'online', '105', '2', 'Balg', 'Balg', 'Balg', 'Balg');
INSERT INTO 'category' VALUES ('107', 'online', '105', '3', 'Ebersteinburg', 'Ebersteinburg', 'Ebersteinburg', 'Ebersteinburg');
INSERT INTO 'category' VALUES ('108', 'online', '105', '4', 'Gaisbach', 'Gaisbach', 'Gaisbach', 'Gaisbach');
INSERT INTO 'category' VALUES ('109', 'online', '105', '5', 'Haueneberstein', 'Haueneberstein', 'Haueneberstein', 'Haueneberstein');
INSERT INTO 'category' VALUES ('110', 'online', '105', '6', 'Lichtental', 'Lichtental', 'Lichtental', 'Lichtental');
INSERT INTO 'category' VALUES ('111', 'online', '105', '7', 'Neuweier', 'Neuweier', 'Neuweier', 'Neuweier');
INSERT INTO 'category' VALUES ('112', 'online', '105', '8', 'Oos', 'Oos', 'Oos', 'Oos');
INSERT INTO 'category' VALUES ('113', 'online', '105', '9', 'Sandweier', 'Sandweier', 'Sandweier', 'Sandweier');
INSERT INTO 'category' VALUES ('114', 'online', '105', '10', 'Steinbach', 'Steinbach', 'Steinbach', 'Steinbach');
INSERT INTO 'category' VALUES ('115', 'online', '105', '11', 'Varnhalt', 'Varnhalt', 'Varnhalt', 'Varnhalt');
INSERT INTO 'category' VALUES ('116', 'online', '1', '12', 'Bâle (CH)', 'Basel (CH)', 'Basel (CH)', 'Basilea (CH)');
INSERT INTO 'category' VALUES ('117', 'online', '116', '13', 'Grossbasel', 'Grossbasel', 'Grossbasel', 'Grossbasel');
INSERT INTO 'category' VALUES ('118', 'online', '116', '14', 'Kleinbasel', 'Kleinbasel', 'Kleinbasel', 'Kleinbasel');
INSERT INTO 'category' VALUES ('119', 'online', '116', '15', 'Altstadt Grossbasel', 'Altstadt Grossbasel', 'Altstadt Grossbasel', 'altstadt Grossbasel');
INSERT INTO 'category' VALUES ('120', 'online', '116', '16', 'Altstadt Kleinbasel', 'Altstadt Kleinbasel', 'Altstadt Kleinbasel', 'altstadt Kleinbasel');
INSERT INTO 'category' VALUES ('121', 'online', '116', '17', 'Am Ring', 'Am Ring', 'am Ring', 'Am Ring');
INSERT INTO 'category' VALUES ('122', 'online', '116', '18', 'Bachletten', 'Bachletten', 'Bachletten', 'Bachletten');
INSERT INTO 'category' VALUES ('123', 'online', '116', '19', 'Breite', 'Breite', 'Breite', 'breite');
INSERT INTO 'category' VALUES ('124', 'online', '116', '20', 'Bruderholz', 'Bruderholz', 'Bruderholz', 'Bruderholz');
INSERT INTO 'category' VALUES ('125', 'online', '116', '21', 'Clara', 'Clara', 'Clara', 'Clara');
INSERT INTO 'category' VALUES ('126', 'online', '116', '22', 'Gotthelf', 'Gotthelf', 'Gotthelf', 'Gotthelf');
INSERT INTO 'category' VALUES ('127', 'online', '116', '23', 'Gundeldingen', 'Gundeldingen', 'Gundeldingen', 'Gundeldingen');
INSERT INTO 'category' VALUES ('128', 'online', '116', '24', 'Hirzbrunnen', 'Hirzbrunnen', 'Hirzbrunnen', 'Hirzbrunnen');
INSERT INTO 'category' VALUES ('129', 'online', '116', '25', 'Iselin', 'Iselin', 'Iselin', 'Iselin');
INSERT INTO 'category' VALUES ('130', 'online', '116', '26', 'Kleinhüningen', 'Kleinhüningen', 'Kleinhüningen', 'Kleinhüningen');
INSERT INTO 'category' VALUES ('131', 'online', '116', '27', 'Klybeck', 'Klybeck', 'Klybeck', 'Klybeck');
INSERT INTO 'category' VALUES ('132', 'online', '116', '28', 'Matthäus', 'Matthäus', 'Matthäus', 'Matthäus');
INSERT INTO 'category' VALUES ('133', 'online', '116', '29', 'Neubad', 'Neubad', 'Neubad', 'Neubad');
INSERT INTO 'category' VALUES ('134', 'online', '116', '30', 'Rosental', 'Rosental', 'Rosental', 'Rosental');
INSERT INTO 'category' VALUES ('135', 'online', '116', '31', 'St. Alban', 'St. Alban', 'St. Alban', 'St. Alban');
INSERT INTO 'category' VALUES ('136', 'online', '116', '32', 'St. Johann', 'St. Johann', 'St. Johann', 'St. Johann');
INSERT INTO 'category' VALUES ('137', 'online', '116', '33', 'Vorstädte', 'Vorstädte', 'Vorstädte', 'Vorstädte');
INSERT INTO 'category' VALUES ('138', 'online', '116', '34', 'Wettstein', 'Wettstein', 'Wettstein', 'Wettstein');
INSERT INTO 'category' VALUES ('139', 'online', '1', '35', 'Bordeaux', 'Bordeaux', 'Bordeaux', 'bordò');
INSERT INTO 'category' VALUES ('140', 'online', '139', '36', 'Capucins-Saint Michel', 'Capuchin Saint-Michel', 'Kapuziner-Saint-Michel', 'Cappuccini Saint- Michel');
INSERT INTO 'category' VALUES ('141', 'online', '139', '37', 'Caudéran- Barrière Judaïque', 'Caudéran Barrier Jewish', 'Caudéran Barrier jüdischen', 'Caudéran Barriera ebraica');
INSERT INTO 'category' VALUES ('142', 'online', '139', '38', 'Grand Parc - Paul Doumergue', 'Grand Park - Paul Doumergue', 'Grand Park - Paul Doumergue', 'Grand Park - Paul Doumergue');
INSERT INTO 'category' VALUES ('143', 'online', '139', '39', 'Hôtel De Ville - quinconce', 'Hotel De Ville - staggered', 'Hotel De Ville - versetzt', 'Hotel De Ville - sfalsati');
INSERT INTO 'category' VALUES ('144', 'online', '139', '40', 'La Bastide', 'La Bastide', 'La Bastide', 'La Bastide');
INSERT INTO 'category' VALUES ('145', 'online', '139', '41', 'Le Lac-Bacalan', 'Lake-Bacalan', 'Lake-Bacalan', 'Lake-Bacalan');
INSERT INTO 'category' VALUES ('146', 'online', '139', '42', 'Saint Bruno', 'Saint Bruno', 'St. Bruno', 'San Bruno');
INSERT INTO 'category' VALUES ('147', 'online', '139', '43', 'Saint Jean- Belcier', 'St. John Belcier', 'St. John Belcier', 'St. John Belcier');
INSERT INTO 'category' VALUES ('148', 'online', '1', '44', 'Lille', 'Lille', 'Lille', 'Lille');
INSERT INTO 'category' VALUES ('149', 'online', '148', '45', 'Centre', 'center', 'Zentrum', 'centro');
INSERT INTO 'category' VALUES ('150', 'online', '148', '46', 'Hypercentre', 'hypercentre', 'Hypercentre', 'hypercentre');
INSERT INTO 'category' VALUES ('151', 'online', '1', '47', 'Marseille', 'Marseilles', 'Marseille', 'Marsiglia');
INSERT INTO 'category' VALUES ('152', 'online', '151', '48', 'Centre', 'center', 'Zentrum', 'centro');
INSERT INTO 'category' VALUES ('153', 'online', '151', '49', 'Hypercentre', 'hypercentre', 'Hypercentre', 'hypercentre');
INSERT INTO 'category' VALUES ('154', 'online', '1', '50', 'Montpellier', 'Montpellier', 'Montpellier', 'Montpellier');
INSERT INTO 'category' VALUES ('155', 'online', '154', '51', 'Centre', 'center', 'Zentrum', 'centro');
INSERT INTO 'category' VALUES ('156', 'online', '154', '52', 'Hypercentre', 'hypercentre', 'Hypercentre', 'hypercentre');
INSERT INTO 'category' VALUES ('157', 'online', '1', '53', 'Mulhouse', 'Mulhouse', 'Mulhouse', 'Mulhouse');
INSERT INTO 'category' VALUES ('158', 'online', '157', '54', 'Centre', 'center', 'Zentrum', 'centro');
INSERT INTO 'category' VALUES ('159', 'online', '157', '55', 'Hypercentre', 'hypercentre', 'Hypercentre', 'hypercentre');
INSERT INTO 'category' VALUES ('160', 'online', '1', '56', 'Rennes', 'Rennes', 'Rennes', 'Rennes');
INSERT INTO 'category' VALUES ('161', 'online', '160', '57', 'Centre', 'center', 'Zentrum', 'centro');
INSERT INTO 'category' VALUES ('162', 'online', '160', '58', 'Hypercentre', 'hypercentre', 'Hypercentre', 'hypercentre');
INSERT INTO 'category' VALUES ('163', 'online', '1', '59', 'Strasbourg', 'Strasbourg', 'Strassburg', 'Strasburgo');
INSERT INTO 'category' VALUES ('164', 'online', '163', '60', 'Centre', 'center', 'Zentrum', 'centro');
INSERT INTO 'category' VALUES ('165', 'online', '163', '61', 'Hypercentre', 'hypercentre', 'Hypercentre', 'hypercentre');
INSERT INTO 'category' VALUES ('166', 'online', '1', '62', 'Toulouse', 'Toulouse', 'Toulouse', 'Tolosa');
INSERT INTO 'category' VALUES ('167', 'online', '166', '63', 'Centre', 'center', 'Zentrum', 'centro');
# --------------------------------------------------------


#
# Structure de la table: category_doc
#
CREATE TABLE 'category_doc' ( 'category' int(10) NOT NULL , 'doc' int(10) NOT NULL );
CREATE UNIQUE INDEX category_doc_ ON 'category_doc' ('category','doc');

#
# Contenu de la table: category_doc
#
# --------------------------------------------------------


#
# Structure de la table: chic_list
#
CREATE TABLE 'chic_list' ( 'id' INTEGER NOT NULL PRIMARY KEY, 'status' varchar(8) NOT NULL DEFAULT 'draft' , 'created' datetime NOT NULL , 'place_cat' int(10) DEFAULT NULL , 'exp_cat' int(10) DEFAULT NULL , 'date_cat' date DEFAULT NULL );

#
# Contenu de la table: chic_list
#
INSERT INTO 'chic_list' VALUES ('1', 'online', '2013-02-24 00:12:31', '163', NULL, '2013-02-25');
# --------------------------------------------------------


#
# Structure de la table: chic_list_doc_master
#
CREATE TABLE 'chic_list_doc_master' ( 'chic_list' int(10) NOT NULL , 'doc_master' int(10) NOT NULL , 'rank' tinyint(4) NOT NULL );
CREATE UNIQUE INDEX chic_list_doc_master_ ON 'chic_list_doc_master' ('chic_list','doc_master');

#
# Contenu de la table: chic_list_doc_master
#
INSERT INTO 'chic_list_doc_master' VALUES ('1', '1', '0');
INSERT INTO 'chic_list_doc_master' VALUES ('1', '2', '1');
INSERT INTO 'chic_list_doc_master' VALUES ('1', '3', '2');
INSERT INTO 'chic_list_doc_master' VALUES ('1', '4', '3');
INSERT INTO 'chic_list_doc_master' VALUES ('1', '5', '4');
INSERT INTO 'chic_list_doc_master' VALUES ('1', '6', '5');
INSERT INTO 'chic_list_doc_master' VALUES ('1', '7', '6');
# --------------------------------------------------------


#
# Structure de la table: doc
#
CREATE TABLE 'doc' ( 'id' INTEGER NOT NULL PRIMARY KEY, 'master' int(10) NOT NULL , 'lang' varchar(2) NOT NULL DEFAULT 'fr' , 'status' varchar(8) NOT NULL DEFAULT 'draft' , 'created' timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP , 'title' varchar(200) NOT NULL , 'subtitle' varchar(200) NOT NULL , 'image' varchar(200) NOT NULL , 'when_txt' text , 'where_txt' text NOT NULL , 'content' text NOT NULL );
CREATE INDEX doc_master_idx ON 'doc'('master');

#
# Contenu de la table: doc
#
INSERT INTO 'doc' VALUES ('2', '4', 'fr', 'online', '2013-02-24 16:40:38', 'Beau linge', 'chez Sarah Verguet', 'image1.jpg', NULL, 'Sonia Verguet.<br/> Le Bastion,<br/> 14 rue des Remparts à Strasbourg', '<h1>Kitsch les torchons alsaciens? Non, chic !</h1> <p>Sonia Verguet signe t-elle ici la fin des tissus Kelsch? Non, mais elle revisite l''imprimé alsacien au travers d''une série de linge de table, nappe et torchons. Diplômé de l''Ecole des Arts Décoratifs de Strasbourg, et aujourd''hui spécialiste du design culinaire, Sonia travaille également le design d''objets. Avec « Folklore », la jeune femme adopte ici les traditions alsaciennes et les modernise par ses lignes épurées. En quelques traits graphiques, elle met à l''honneur dans cette collection les maisons à colombages. C''est moderne, simple et chic ! Malheureusement, Sonia Verguet n''a pas encore trouvé de distributeurs pour ces torchons. En attendant on peut aller directement les acheter dans son atelier, au Bastion. L''occasion de découvrir ses autres créations, très inspirées par l''Alsace.</p>');
INSERT INTO 'doc' VALUES ('3', '2', 'fr', 'online', '2013-02-24 16:30:26', 'Cours de cuisine : voyage au Maroc', 'Atelier des chefs', 'image2.jpg', '10h15', 'Atelier des chefs', '<h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h1> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris iaculis cursus diam sit amet cursus. Duis eget nibh ut felis lacinia iaculis eu in enim. Morbi feugiat convallis ante. Vivamus non sodales ante. Fusce nec nisl augue, ut volutpat est. Praesent consequat, nibh ac hendrerit posuere, lectus augue lacinia turpis, ut imperdiet tortor ligula et massa. Etiam dui neque, convallis at semper eu, porta a magna. Nulla facilisi. Donec convallis pellentesque imperdiet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed ultricies, turpis eu commodo aliquet, diam mauris mattis neque, sit amet volutpat ligula leo id dolor. In hac habitasse platea dictumst. Etiam sit amet mollis justo.</p>');
INSERT INTO 'doc' VALUES ('4', '7', 'fr', 'online', '2013-02-24 16:37:28', 'Dr. CAC : Apprenez l''économie sans souci', 'France 5', 'image7.jpg', '20h20', '', '<h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h1> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris iaculis cursus diam sit amet cursus. Duis eget nibh ut felis lacinia iaculis eu in enim. Morbi feugiat convallis ante. Vivamus non sodales ante. Fusce nec nisl augue, ut volutpat est. Praesent consequat, nibh ac hendrerit posuere, lectus augue lacinia turpis, ut imperdiet tortor ligula et massa. Etiam dui neque, convallis at semper eu, porta a magna. Nulla facilisi. Donec convallis pellentesque imperdiet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed ultricies, turpis eu commodo aliquet, diam mauris mattis neque, sit amet volutpat ligula leo id dolor. In hac habitasse platea dictumst. Etiam sit amet mollis justo.</p>');
INSERT INTO 'doc' VALUES ('5', '1', 'fr', 'online', '2013-02-24 16:39:09', 'Journée Day Spa Bio', 'La Clairière', 'image4.jpg', NULL, 'La Clairière', '<h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </h1> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris iaculis cursus diam sit amet cursus. Duis eget nibh ut felis lacinia iaculis eu in enim. Morbi feugiat convallis ante. Vivamus non sodales ante. Fusce nec nisl augue, ut volutpat est. Praesent consequat, nibh ac hendrerit posuere, lectus augue lacinia turpis, ut imperdiet tortor ligula et massa. Etiam dui neque, convallis at semper eu, porta a magna. Nulla facilisi. Donec convallis pellentesque imperdiet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed ultricies, turpis eu commodo aliquet, diam mauris mattis neque, sit amet volutpat ligula leo id dolor. In hac habitasse platea dictumst. Etiam sit amet mollis justo.</p>');
INSERT INTO 'doc' VALUES ('6', '6', 'fr', 'online', '2013-02-24 16:37:28', 'Tu honoreras ta mère et ta mère', 'Cinéma Star', 'image6.jpg', '20h15', '', '<h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h1> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris iaculis cursus diam sit amet cursus. Duis eget nibh ut felis lacinia iaculis eu in enim. Morbi feugiat convallis ante. Vivamus non sodales ante. Fusce nec nisl augue, ut volutpat est. Praesent consequat, nibh ac hendrerit posuere, lectus augue lacinia turpis, ut imperdiet tortor ligula et massa. Etiam dui neque, convallis at semper eu, porta a magna. Nulla facilisi. Donec convallis pellentesque imperdiet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed ultricies, turpis eu commodo aliquet, diam mauris mattis neque, sit amet volutpat ligula leo id dolor. In hac habitasse platea dictumst. Etiam sit amet mollis justo.</p>');
INSERT INTO 'doc' VALUES ('7', '5', 'fr', 'online', '2013-02-24 16:37:28', 'Le Restaurant Savoyard', 'Etoile des neiges', 'image5.jpg', '19h00', 'Etoile des neiges', '<h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h1> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris iaculis cursus diam sit amet cursus. Duis eget nibh ut felis lacinia iaculis eu in enim. Morbi feugiat convallis ante. Vivamus non sodales ante. Fusce nec nisl augue, ut volutpat est. Praesent consequat, nibh ac hendrerit posuere, lectus augue lacinia turpis, ut imperdiet tortor ligula et massa. Etiam dui neque, convallis at semper eu, porta a magna. Nulla facilisi. Donec convallis pellentesque imperdiet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed ultricies, turpis eu commodo aliquet, diam mauris mattis neque, sit amet volutpat ligula leo id dolor. In hac habitasse platea dictumst. Etiam sit amet mollis justo.</p>');
INSERT INTO 'doc' VALUES ('8', '3', 'fr', 'online', '2013-02-24 16:37:28', 'Visite guidée gratuite de Strasbourg', 'Local Trotter', 'image3.jpg', '14h00', 'Local Trotter<br/> Place de cathédrale', '<h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </h1> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris iaculis cursus diam sit amet cursus. Duis eget nibh ut felis lacinia iaculis eu in enim. Morbi feugiat convallis ante. Vivamus non sodales ante. Fusce nec nisl augue, ut volutpat est. Praesent consequat, nibh ac hendrerit posuere, lectus augue lacinia turpis, ut imperdiet tortor ligula et massa. Etiam dui neque, convallis at semper eu, porta a magna. Nulla facilisi. Donec convallis pellentesque imperdiet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed ultricies, turpis eu commodo aliquet, diam mauris mattis neque, sit amet volutpat ligula leo id dolor. In hac habitasse platea dictumst. Etiam sit amet mollis justo.</p>');
# --------------------------------------------------------


#
# Structure de la table: doc_master
#
CREATE TABLE 'doc_master' ( 'id' INTEGER NOT NULL PRIMARY KEY, 'target' varchar(50) NOT NULL , 'type' varchar(6) NOT NULL DEFAULT 'billet' , 'place_cat' int(10) NOT NULL , 'exp_cat' int(10) NOT NULL , 'exp_scat' int(10) DEFAULT NULL , 'author' int(10) NOT NULL , 'chic_clics' int(10) NOT NULL );
CREATE UNIQUE INDEX doc_master_target ON 'doc_master' ('target');
CREATE INDEX doc_master_urlKey ON 'doc_master' ('target');

#
# Contenu de la table: doc_master
#
INSERT INTO 'doc_master' VALUES ('1', 'journ- spa-bio', 'billet', '163', '57', '60', '1', '12');
INSERT INTO 'doc_master' VALUES ('2', 'cuis- mar', 'billet', '163', '99', '103', '1', '12');
INSERT INTO 'doc_master' VALUES ('3', 'vis- grat-stras', 'billet', '163', '99', '100', '1', '0');
INSERT INTO 'doc_master' VALUES ('4', 'beau- ling', 'billet', '163', '39', '41', '1', '67');
INSERT INTO 'doc_master' VALUES ('5', 'rest- sav', 'billet', '163', '3', '4', '1', '89');
INSERT INTO 'doc_master' VALUES ('6', 'tu-hon- ta-mer', 'billet', '163', '94', NULL, '1', '89');
INSERT INTO 'doc_master' VALUES ('7', 'drCac', 'billet', '163', '99', '101', '1', '89');
# --------------------------------------------------------


#
# Structure de la table: user
#
CREATE TABLE 'user' ( 'id' INTEGER NOT NULL PRIMARY KEY, 'label' varchar(100) NOT NULL , 'email' varchar(255) NOT NULL , 'password' varchar(30) NOT NULL , 'created' datetime NOT NULL , 'last_connected' datetime NOT NULL , 'status' varchar(7) NOT NULL , 'connection_serie' int(11) DEFAULT NULL , 'connection_token' int(11) DEFAULT NULL );

#
# Contenu de la table: user
#
INSERT INTO 'user' VALUES ('1', 'Chic-List Admin', 'chiclistadmin', 'test', '2013-02-09 15:45:56', '2013-02-09 15:45:56', 'admin', NULL, NULL);
INSERT INTO 'user' VALUES ('2', 'Pierre W', 'orden42@gmail.com', 'test', '2013-02-09 15:46:24', '2013-02-09 15:46:24', 'admin', NULL, NULL);
INSERT INTO 'user' VALUES ('3', 'Alfred 1', 'alfred1@test.com', 'test', '2013-02-09 15:47:21', '2013-02-09 15:47:21', 'online', NULL, NULL);
INSERT INTO 'user' VALUES ('4', 'Bernard 1', 'bernard1@test.com', 'test', '2013-02-09 15:49:15', '2013-02-09 15:49:15', 'online', NULL, NULL);
INSERT INTO 'user' VALUES ('5', 'Charles 1', 'charles1@test.com', 'test', '2013-02-09 15:49:15', '2013-02-09 15:49:15', 'online', NULL, NULL);
INSERT INTO 'user' VALUES ('6', 'Denis 1', 'denis1@test.com', 'test', '2013-02-09 15:49:15', '2013-02-09 15:49:15', 'online', NULL, NULL);
INSERT INTO 'user' VALUES ('7', 'Edouard 1', 'edouard1@test.com', 'test', '2013-02-09 15:49:15', '2013-02-09 15:49:15', 'online', NULL, NULL);
INSERT INTO 'user' VALUES ('8', 'François 1', 'francois1@test.com', 'test', '2013-02-09 15:49:15', '2013-02-09 15:49:15', 'online', NULL, NULL);
INSERT INTO 'user' VALUES ('9', 'Gérard 1', 'gerard1@test.com', 'test', '2013-02-09 15:49:15', '2013-02-09 15:49:15', 'online', NULL, NULL);
INSERT INTO 'user' VALUES ('10', 'Henri 1', 'henri1@test.com', 'test', '2013-02-09 15:49:15', '2013-02-09 15:49:15', 'online', NULL, NULL);
# --------------------------------------------------------