-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Lun 25 Février 2013 à 16:06
-- Version du serveur: 5.5.24-log
-- Version de PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `chiclist`
--

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` enum('draft','review','approved','online','rejected','obsolete','canceled') COLLATE utf8_bin NOT NULL DEFAULT 'draft',
  `parent_cat` int(10) unsigned DEFAULT NULL,
  `rank` tinyint(3) unsigned NOT NULL,
  `label_fr` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `label_en` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `label_de` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `label_it` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_cat` (`parent_cat`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=168 ;

--
-- Contenu de la table `category`
--

INSERT INTO `category` (`id`, `status`, `parent_cat`, `rank`, `label_fr`, `label_en`, `label_de`, `label_it`) VALUES
(1, 'online', NULL, 0, 'OU', NULL, NULL, NULL),
(2, 'online', NULL, 0, 'EXP', NULL, NULL, NULL),
(3, 'online', 2, 1, 'Boire et manger', 'Food and drink', 'Essen und Trinken', 'Cibo e bevande'),
(4, 'online', 3, 2, 'repas léger', 'light meal', 'leichte Mahlzeit', 'pasto leggero'),
(5, 'online', 3, 3, 'dégustation', 'tasting', 'Weinprobe', 'degustazione'),
(6, 'online', 3, 4, 'vins fins et spiritueux', 'wines and spirits', 'Weine und Spirituosen', 'vini e liquori'),
(7, 'online', 3, 5, 'bonbons &amp; chocolats', 'sweets & chocolates', 'Süßigkeiten und Schokolade', 'caramelle e cioccolatini'),
(8, 'online', 3, 6, 'pique-nique', 'picnic', 'Picknick', 'picnic'),
(9, 'online', 3, 7, 'traiteur, épicerie fine', 'catering, delicatessen', 'Catering, Delikatessen', 'catering, salumeria'),
(10, 'online', 3, 8, 'souper raffiné', 'refined dinner', 'raffiniertes Dinner', 'cena raffinata'),
(11, 'online', 3, 9, 'dîner aux chandelles', 'candlelight dinner', 'Candlelight-Dinner', 'a lume di candela cena'),
(12, 'online', 3, 10, 'thé ou café ?', 'Tea or coffee?', 'Tee oder Kaffee?', 'Tè o caffè?'),
(13, 'online', 3, 11, 'sur le pouce', 'on the go', 'auf dem Sprung', 'in movimento'),
(14, 'online', 3, 12, 'apéros &amp; afterworks', 'appetizers & Afterworks', 'Vorspeisen & Afterworks', 'antipasti e Afterworks'),
(15, 'online', 3, 13, 'petit-déjeuner, brunch', 'breakfast, brunch', 'Frühstück, Brunch', 'colazione, il brunch'),
(16, 'online', 3, 14, 'terrasse et plein air', 'terrace and outdoor', 'Terrasse und Outdoor', 'terrazza e spazi esterni'),
(17, 'online', 3, 15, 'fruits de mer', 'seafood', 'Meeresfrüchte', 'frutti di mare'),
(18, 'online', 3, 16, 'alsacien', 'Alsatian', 'Elsässer', 'alsaziano'),
(19, 'online', 3, 17, 'entre amis', 'friends', 'Freunden', 'amici'),
(20, 'online', 3, 18, 'dîner spectacle', 'dinner show', 'Dinner-Show', 'cena spettacolo'),
(21, 'online', 3, 19, 'pâtisseries', 'pastries', 'Gebäck', 'pasticceria'),
(22, 'online', 3, 20, 'pour les becs sucrés', 'for the sweet tooth', 'für den süßen Zahn', 'per i più golosi'),
(23, 'online', 3, 21, 'au marché', 'market', 'Markt', 'mercato'),
(24, 'online', 3, 22, 'un chef à la maison', 'a leader at home', 'führend zu Hause', 'un leader in casa'),
(25, 'online', 3, 23, 'livraison à domicile', 'home delivery', 'Hauszustellung', 'consegna a domicilio'),
(26, 'online', 3, 24, 'la recette du jour', 'Recipe of the day', 'Rezept des Tages', 'Ricetta del giorno'),
(27, 'online', 3, 25, 'à emporter', 'Takeaway', 'Takeaway', 'da asporto'),
(28, 'online', 2, 26, 'Dormir', 'sleep', 'schlafen', 'dormire'),
(29, 'online', 28, 27, 'chambre d''hôtes', 'guest', 'Gast', 'ospite'),
(30, 'online', 28, 28, 'au centre ville', 'downtown', 'Innenstadt', 'centro'),
(31, 'online', 28, 29, 'charmant hôtel', 'charming', 'charmant', 'affascinante'),
(32, 'online', 2, 30, 'Être solidaire', 'be secured', 'gesichert werden', 'essere assicurato'),
(33, 'online', 32, 31, 'être bénévole', 'be a volunteer', 'sein ein Freiwilliger', 'essere un volontario'),
(34, 'online', 32, 32, 'donner, partager', 'give, share', 'geben, Aktien', 'dare, condividere'),
(35, 'online', 32, 33, 'voyager', 'travel', 'reisen', 'viaggiare'),
(36, 'online', 32, 34, 's''engager', 'engage', 'engagieren', 'impegnarsi'),
(37, 'online', 32, 35, 's''informer', 'learn', 'lernen', 'imparare'),
(38, 'online', 32, 36, 'la vie en vert', 'green living', 'grüne Wohnzimmer', 'vivere verde'),
(39, 'online', 2, 37, 'Faire du shopping', 'Go shopping', 'Einkaufen gehen', 'Andare a fare shopping'),
(40, 'online', 39, 38, 'chic &amp; cheap', 'chic and cheap', 'chic und günstig', 'chic and cheap'),
(41, 'online', 39, 39, 'meubles, objets et design', 'furniture, and design', 'Möbel und Design', 'mobili, e il design'),
(42, 'online', 39, 40, 'essentiels accessoires', 'essential accessories', 'notwendiges Zubehör', 'accessori indispensabili'),
(43, 'online', 39, 41, 'chaussures et bagagerie', 'shoes and luggage', 'Schuhe und Gepäck', 'calzature e articoli da viaggio'),
(44, 'online', 39, 42, 'nature et sport', 'nature and sport', 'Natur und Sport', 'natura e sport'),
(45, 'online', 39, 43, 'sport &amp; streetwear', 'sports & streetwear', 'sports & Streetwear', 'sport & streetwear'),
(46, 'online', 39, 44, 'belles plantes', 'beautiful plants', 'schöne Pflanzen', 'belle piante'),
(47, 'online', 39, 45, 'dessous chics', 'dessous chics', 'dessous chics', 'dessous chics'),
(48, 'online', 39, 46, 'chic &amp; décontracté', 'chic & casual', 'chic & casual', 'chic e casual'),
(49, 'online', 39, 47, 'éternel féminin', 'eternal feminine', 'ewig-Weiblichen', 'eterno femminino'),
(50, 'online', 39, 48, 'dandys', 'dandies', 'Dandys', 'dandy'),
(51, 'online', 39, 49, 'jeux et jouets', 'games and toys', 'Spiele und Spielzeug', 'giochi e giocattoli'),
(52, 'online', 39, 50, 'parfums et bijoux', 'perfume and jewelry', 'Parfüm und Schmuck', 'profumi e gioielli'),
(53, 'online', 39, 51, 'musique maestro !', 'music maestro!', 'Musik maestro!', 'musica maestro!'),
(54, 'online', 39, 52, 'l''heure des créateurs', 'creative time', 'kreative Zeit', 'momento creativo'),
(55, 'online', 39, 53, 'idées de cadeau', 'gift ideas', 'Geschenkideen', 'idee regalo'),
(56, 'online', 39, 54, 'livres', 'books', 'Bücher', 'libri'),
(57, 'online', 2, 55, 'Prendre soin de soi', 'Taking care of yourself', 'Die Pflege von sich selbst', 'Prendersi cura di se stessi'),
(58, 'online', 57, 56, 'activités physiques douces', 'mild physical activity', 'milde körperliche Aktivität', 'lieve attività fisica'),
(59, 'online', 57, 57, 'sport', 'sport', 'Sport', 'sport'),
(60, 'online', 57, 58, 'soins du corps', 'body care', 'Körperpflege', 'cura del corpo'),
(61, 'online', 57, 59, 'spa et thermes', 'spa and spa', 'Spa und Wellness', 'spa e centro benessere'),
(62, 'online', 57, 60, 'se faire coiffer, maquiller', 'their hair done, makeup', 'ihr Haar getan, Make-up', 'i loro capelli fatti, trucco'),
(63, 'online', 57, 61, 'se faire masser', 'get a massage', 'eine Massage', 'ottenere un massaggio'),
(64, 'online', 57, 62, 'manucure', 'manicure', 'Maniküre', 'manicure'),
(65, 'online', 57, 63, 'faire du sport', 'make sport', 'machen Sport', 'fare sport'),
(66, 'online', 57, 64, 'se relaxer', 'relax', 'entspannen', 'relax'),
(67, 'online', 2, 65, 'Rester chez soi', 'Stay at home', 'Bleiben Sie zu Hause', 'Stai a casa'),
(68, 'online', 67, 66, 'télé, vidéos &amp; VOD', 'TV, VOD & videos', 'TV, VOD und Videos', 'TV, VOD & video'),
(69, 'online', 67, 67, 'bons bouquins', 'good books', 'gute Bücher', 'buoni libri'),
(70, 'online', 67, 68, 'à la radio…', 'on the radio ...', 'auf der radio ...', 'alla radio ...'),
(71, 'online', 67, 69, 'rien à cuisiner', 'no cooking', 'kein Kochen', 'non cucina'),
(72, 'online', 67, 70, 'cultiver son jardin ou son balcon', 'cultivate his garden or balcony', 'kultivieren seinem Garten oder Balkon', 'coltivare il suo giardino o balcone'),
(73, 'online', 67, 71, 'bricoler', 'tinker', 'basteln', 'rattoppare'),
(74, 'online', 67, 72, 'le bon bouquin', 'the good book', 'das gute Buch', 'il buon libro'),
(75, 'online', 67, 73, 'acheter sur Internet', 'buy on Internet', 'kaufen im Internet', 'acquistare su Internet'),
(76, 'online', 2, 74, 'S’occuper des enfants', 'Caring for children', 'Betreuung von Kindern', 'Prendersi cura di bambini'),
(77, 'online', 76, 75, 'contes', 'tales', 'Tales', 'Tales'),
(78, 'online', 76, 76, 'les habiller', 'dress up', 'verkleiden', 'vestire'),
(79, 'online', 76, 77, 'les activité du mercredis', 'the activity of Wednesdays', 'die Aktivität des Mittwochs', 'l''attività di mercoledì'),
(80, 'online', 76, 78, 'la ferme !', 'shut up!', 'shut up!', 'stai zitto!'),
(81, 'online', 76, 79, 'plein air', 'outdoor', 'im Freien', 'all''aperto'),
(82, 'online', 76, 80, 'artistes en herbe', 'budding artists', 'angehende Künstler', 'artisti in erba'),
(83, 'online', 76, 81, 'spectacles', 'sights', 'Sehenswürdigkeiten', 'Attrazioni'),
(84, 'online', 76, 82, 'apprendre en s''amusant', 'learning fun', 'Lernen Spaß', 'divertente l''apprendimento'),
(85, 'online', 76, 83, 'cirque', 'circus', 'Zirkus', 'circo'),
(86, 'online', 76, 84, 'activités sportives', 'sports', 'Sport', 'dello sport'),
(87, 'online', 2, 85, 'S’organiser', 'organize', 'organisieren', 'organizzare'),
(88, 'online', 87, 86, 'coup d''aspiro', 'coup d''aspiro', 'coup d''Aspiro', 'colpo di ASPIRO'),
(89, 'online', 87, 87, 'louer', 'rent', 'mieten', 'affitto'),
(90, 'online', 87, 88, 'services publics', 'utilities', 'Dienstprogramme', 'programmi di utilità'),
(91, 'online', 87, 89, 'se faire livrer ses courses', 'be delivering his races', 'ausliefern seine Rennen', 'consegnerà le sue gare'),
(92, 'online', 87, 90, 'baby-sitting et garde d''enfants', 'babysitting and childcare', 'Babysitting und Kinderbetreuung', 'baby-sitter e assistenza all''infanzia'),
(93, 'online', 87, 91, 'petits et grands travaux', 'large and small works', 'große und kleine Werke', 'opere di grandi e piccole'),
(94, 'online', 2, 92, 'Sortir', 'exit', 'verlassen', 'uscita'),
(95, 'online', 94, 93, 'au théâtre ce soir', 'theater tonight', 'Theater heute', 'teatro stasera'),
(96, 'online', 94, 94, 'concerts et festivals', 'concerts and festivals', 'Konzerte und Festivals', 'concerti e festival'),
(97, 'online', 94, 95, 'art lyrique et opéra', 'opera and opera', 'Oper und Oper', 'opera e opera'),
(98, 'online', 94, 96, 'danse', 'dance', 'tanzen', 'ballare'),
(99, 'online', 2, 97, 'Voir et savoir', 'Look and learn', 'Schauen und lernen', 'Guarda e impara'),
(100, 'online', 99, 98, 'architecture et patrimoine', 'Heritage and architecture', 'Heritage und Architektur', 'Heritage e architettura'),
(101, 'online', 99, 99, 'conférences', 'conferences', 'Konferenzen', 'conferenze'),
(102, 'online', 99, 100, 'expositions temporaires', 'exhibitions', 'Ausstellungen', 'mostre'),
(103, 'online', 99, 101, 'apprendre à...', 'learn how to ...', 'lernen, wie man ...', 'imparare a ...'),
(104, 'online', 99, 102, 'musées', 'museums', 'Museen', 'musei'),
(105, 'online', 1, 1, 'Baden-Baden (DE)', 'Baden-Baden (DE)', 'Baden-Baden', 'Baden-Baden (DE)'),
(106, 'online', 105, 2, 'Balg', 'Balg', 'Balg', 'Balg'),
(107, 'online', 105, 3, 'Ebersteinburg', 'Ebersteinburg', 'Ebersteinburg', 'Ebersteinburg'),
(108, 'online', 105, 4, 'Gaisbach', 'Gaisbach', 'Gaisbach', 'Gaisbach'),
(109, 'online', 105, 5, 'Haueneberstein', 'Haueneberstein', 'Haueneberstein', 'Haueneberstein'),
(110, 'online', 105, 6, 'Lichtental', 'Lichtental', 'Lichtental', 'Lichtental'),
(111, 'online', 105, 7, 'Neuweier', 'Neuweier', 'Neuweier', 'Neuweier'),
(112, 'online', 105, 8, 'Oos', 'Oos', 'Oos', 'Oos'),
(113, 'online', 105, 9, 'Sandweier', 'Sandweier', 'Sandweier', 'Sandweier'),
(114, 'online', 105, 10, 'Steinbach', 'Steinbach', 'Steinbach', 'Steinbach'),
(115, 'online', 105, 11, 'Varnhalt', 'Varnhalt', 'Varnhalt', 'Varnhalt'),
(116, 'online', 1, 12, 'Bâle (CH)', 'Basel (CH)', 'Basel (CH)', 'Basilea (CH)'),
(117, 'online', 116, 13, 'Grossbasel', 'Grossbasel', 'Grossbasel', 'Grossbasel'),
(118, 'online', 116, 14, 'Kleinbasel', 'Kleinbasel', 'Kleinbasel', 'Kleinbasel'),
(119, 'online', 116, 15, 'Altstadt Grossbasel', 'Altstadt Grossbasel', 'Altstadt Grossbasel', 'altstadt Grossbasel'),
(120, 'online', 116, 16, 'Altstadt Kleinbasel', 'Altstadt Kleinbasel', 'Altstadt Kleinbasel', 'altstadt Kleinbasel'),
(121, 'online', 116, 17, 'Am Ring', 'Am Ring', 'am Ring', 'Am Ring'),
(122, 'online', 116, 18, 'Bachletten', 'Bachletten', 'Bachletten', 'Bachletten'),
(123, 'online', 116, 19, 'Breite', 'Breite', 'Breite', 'breite'),
(124, 'online', 116, 20, 'Bruderholz', 'Bruderholz', 'Bruderholz', 'Bruderholz'),
(125, 'online', 116, 21, 'Clara', 'Clara', 'Clara', 'Clara'),
(126, 'online', 116, 22, 'Gotthelf', 'Gotthelf', 'Gotthelf', 'Gotthelf'),
(127, 'online', 116, 23, 'Gundeldingen', 'Gundeldingen', 'Gundeldingen', 'Gundeldingen'),
(128, 'online', 116, 24, 'Hirzbrunnen', 'Hirzbrunnen', 'Hirzbrunnen', 'Hirzbrunnen'),
(129, 'online', 116, 25, 'Iselin', 'Iselin', 'Iselin', 'Iselin'),
(130, 'online', 116, 26, 'Kleinhüningen', 'Kleinhüningen', 'Kleinhüningen', 'Kleinhüningen'),
(131, 'online', 116, 27, 'Klybeck', 'Klybeck', 'Klybeck', 'Klybeck'),
(132, 'online', 116, 28, 'Matthäus', 'Matthäus', 'Matthäus', 'Matthäus'),
(133, 'online', 116, 29, 'Neubad', 'Neubad', 'Neubad', 'Neubad'),
(134, 'online', 116, 30, 'Rosental', 'Rosental', 'Rosental', 'Rosental'),
(135, 'online', 116, 31, 'St. Alban', 'St. Alban', 'St. Alban', 'St. Alban'),
(136, 'online', 116, 32, 'St. Johann', 'St. Johann', 'St. Johann', 'St. Johann'),
(137, 'online', 116, 33, 'Vorstädte', 'Vorstädte', 'Vorstädte', 'Vorstädte'),
(138, 'online', 116, 34, 'Wettstein', 'Wettstein', 'Wettstein', 'Wettstein'),
(139, 'online', 1, 35, 'Bordeaux', 'Bordeaux', 'Bordeaux', 'bordò'),
(140, 'online', 139, 36, 'Capucins-Saint Michel', 'Capuchin Saint-Michel', 'Kapuziner-Saint-Michel', 'Cappuccini Saint-Michel'),
(141, 'online', 139, 37, 'Caudéran-Barrière Judaïque', 'Caudéran Barrier Jewish', 'Caudéran Barrier jüdischen', 'Caudéran Barriera ebraica'),
(142, 'online', 139, 38, 'Grand Parc - Paul Doumergue', 'Grand Park - Paul Doumergue', 'Grand Park - Paul Doumergue', 'Grand Park - Paul Doumergue'),
(143, 'online', 139, 39, 'Hôtel De Ville - quinconce', 'Hotel De Ville - staggered', 'Hotel De Ville - versetzt', 'Hotel De Ville - sfalsati'),
(144, 'online', 139, 40, 'La Bastide', 'La Bastide', 'La Bastide', 'La Bastide'),
(145, 'online', 139, 41, 'Le Lac-Bacalan', 'Lake-Bacalan', 'Lake-Bacalan', 'Lake-Bacalan'),
(146, 'online', 139, 42, 'Saint Bruno', 'Saint Bruno', 'St. Bruno', 'San Bruno'),
(147, 'online', 139, 43, 'Saint Jean-Belcier', 'St. John Belcier', 'St. John Belcier', 'St. John Belcier'),
(148, 'online', 1, 44, 'Lille', 'Lille', 'Lille', 'Lille'),
(149, 'online', 148, 45, 'Centre', 'center', 'Zentrum', 'centro'),
(150, 'online', 148, 46, 'Hypercentre', 'hypercentre', 'Hypercentre', 'hypercentre'),
(151, 'online', 1, 47, 'Marseille', 'Marseilles', 'Marseille', 'Marsiglia'),
(152, 'online', 151, 48, 'Centre', 'center', 'Zentrum', 'centro'),
(153, 'online', 151, 49, 'Hypercentre', 'hypercentre', 'Hypercentre', 'hypercentre'),
(154, 'online', 1, 50, 'Montpellier', 'Montpellier', 'Montpellier', 'Montpellier'),
(155, 'online', 154, 51, 'Centre', 'center', 'Zentrum', 'centro'),
(156, 'online', 154, 52, 'Hypercentre', 'hypercentre', 'Hypercentre', 'hypercentre'),
(157, 'online', 1, 53, 'Mulhouse', 'Mulhouse', 'Mulhouse', 'Mulhouse'),
(158, 'online', 157, 54, 'Centre', 'center', 'Zentrum', 'centro'),
(159, 'online', 157, 55, 'Hypercentre', 'hypercentre', 'Hypercentre', 'hypercentre'),
(160, 'online', 1, 56, 'Rennes', 'Rennes', 'Rennes', 'Rennes'),
(161, 'online', 160, 57, 'Centre', 'center', 'Zentrum', 'centro'),
(162, 'online', 160, 58, 'Hypercentre', 'hypercentre', 'Hypercentre', 'hypercentre'),
(163, 'online', 1, 59, 'Strasbourg', 'Strasbourg', 'Strassburg', 'Strasburgo'),
(164, 'online', 163, 60, 'Centre', 'center', 'Zentrum', 'centro'),
(165, 'online', 163, 61, 'Hypercentre', 'hypercentre', 'Hypercentre', 'hypercentre'),
(166, 'online', 1, 62, 'Toulouse', 'Toulouse', 'Toulouse', 'Tolosa'),
(167, 'online', 166, 63, 'Centre', 'center', 'Zentrum', 'centro');

-- --------------------------------------------------------

--
-- Structure de la table `category_doc`
--

CREATE TABLE IF NOT EXISTS `category_doc` (
  `category` int(10) unsigned NOT NULL,
  `doc` int(10) unsigned NOT NULL,
  PRIMARY KEY (`category`,`doc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `chic_list`
--

CREATE TABLE IF NOT EXISTS `chic_list` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` enum('draft','review','approved','online','rejected','obsolete','canceled') COLLATE utf8_bin NOT NULL DEFAULT 'draft',
  `created` datetime NOT NULL,
  `place_cat` int(10) unsigned DEFAULT NULL,
  `exp_cat` int(10) unsigned DEFAULT NULL,
  `date_cat` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

--
-- Contenu de la table `chic_list`
--

INSERT INTO `chic_list` (`id`, `status`, `created`, `place_cat`, `exp_cat`, `date_cat`) VALUES
(1, 'online', '2013-02-24 00:12:31', 163, NULL, '2013-02-25');

-- --------------------------------------------------------

--
-- Structure de la table `chic_list_doc_master`
--

CREATE TABLE IF NOT EXISTS `chic_list_doc_master` (
  `chic_list` int(10) unsigned NOT NULL,
  `doc_master` int(10) unsigned NOT NULL,
  `rank` tinyint(4) NOT NULL,
  PRIMARY KEY (`chic_list`,`doc_master`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `chic_list_doc_master`
--

INSERT INTO `chic_list_doc_master` (`chic_list`, `doc_master`, `rank`) VALUES
(1, 1, 0),
(1, 2, 1),
(1, 3, 2),
(1, 4, 3),
(1, 5, 4),
(1, 6, 5),
(1, 7, 6);

-- --------------------------------------------------------

--
-- Structure de la table `doc`
--

CREATE TABLE IF NOT EXISTS `doc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `master` int(10) unsigned NOT NULL,
  `lang` enum('fr','de','en','it') COLLATE utf8_bin NOT NULL DEFAULT 'fr',
  `status` enum('draft','review','approved','online','rejected','obsolete','canceled') COLLATE utf8_bin NOT NULL DEFAULT 'draft',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `title` varchar(200) COLLATE utf8_bin NOT NULL,
  `subtitle` varchar(200) COLLATE utf8_bin NOT NULL,
  `image` varchar(200) COLLATE utf8_bin NOT NULL,
  `when_txt` text COLLATE utf8_bin,
  `where_txt` text COLLATE utf8_bin NOT NULL,
  `content` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `urlKey` (`master`),
  KEY `master` (`master`),
  KEY `master_2` (`master`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=9 ;

--
-- Contenu de la table `doc`
--

INSERT INTO `doc` (`id`, `master`, `lang`, `status`, `created`, `title`, `subtitle`, `image`, `when_txt`, `where_txt`, `content`) VALUES
(2, 4, 'fr', 'online', '2013-02-24 16:40:38', 'Beau linge', 'chez Sarah Verguet', 'image1.jpg', NULL, 'Sonia Verguet.<br/>\r\nLe Bastion,<br/>\r\n14 rue des Remparts à Strasbourg', '<h1>Kitsch les torchons alsaciens? Non, chic !</h1>\r\n\r\n<p>Sonia Verguet signe t-elle ici la fin des tissus Kelsch? Non, mais elle revisite l''imprimé alsacien au travers d''une série de linge de table, nappe et torchons. Diplômé de l''Ecole des Arts Décoratifs de Strasbourg, et aujourd''hui spécialiste du design culinaire, Sonia travaille également le design d''objets. Avec « Folklore », la jeune femme adopte ici les traditions alsaciennes et les modernise par ses lignes épurées. En quelques traits graphiques, elle met à l''honneur dans cette collection les maisons à colombages. C''est moderne, simple et chic ! Malheureusement, Sonia Verguet n''a pas encore trouvé de distributeurs pour ces torchons. En attendant on peut aller directement les acheter dans son atelier, au Bastion. L''occasion de découvrir ses autres créations, très inspirées par l''Alsace.</p>'),
(3, 2, 'fr', 'online', '2013-02-24 16:30:26', 'Cours de cuisine : voyage au Maroc', 'Atelier des chefs', 'image2.jpg', '10h15', 'Atelier des chefs', '<h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h1>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris iaculis cursus diam sit amet cursus. Duis eget nibh ut felis lacinia iaculis eu in enim. Morbi feugiat convallis ante. Vivamus non sodales ante. Fusce nec nisl augue, ut volutpat est. Praesent consequat, nibh ac hendrerit posuere, lectus augue lacinia turpis, ut imperdiet tortor ligula et massa. Etiam dui neque, convallis at semper eu, porta a magna. Nulla facilisi. Donec convallis pellentesque imperdiet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed ultricies, turpis eu commodo aliquet, diam mauris mattis neque, sit amet volutpat ligula leo id dolor. In hac habitasse platea dictumst. Etiam sit amet mollis justo.</p>'),
(4, 7, 'fr', 'online', '2013-02-24 16:37:28', 'Dr. CAC : Apprenez l''économie sans souci', 'France 5', 'image7.jpg', '20h20', '', '<h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h1>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris iaculis cursus diam sit amet cursus. Duis eget nibh ut felis lacinia iaculis eu in enim. Morbi feugiat convallis ante. Vivamus non sodales ante. Fusce nec nisl augue, ut volutpat est. Praesent consequat, nibh ac hendrerit posuere, lectus augue lacinia turpis, ut imperdiet tortor ligula et massa. Etiam dui neque, convallis at semper eu, porta a magna. Nulla facilisi. Donec convallis pellentesque imperdiet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed ultricies, turpis eu commodo aliquet, diam mauris mattis neque, sit amet volutpat ligula leo id dolor. In hac habitasse platea dictumst. Etiam sit amet mollis justo.</p>'),
(5, 1, 'fr', 'online', '2013-02-24 16:39:09', 'Journée Day Spa Bio', 'La Clairière', 'image4.jpg', NULL, 'La Clairière', '<h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h1>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris iaculis cursus diam sit amet cursus. Duis eget nibh ut felis lacinia iaculis eu in enim. Morbi feugiat convallis ante. Vivamus non sodales ante. Fusce nec nisl augue, ut volutpat est. Praesent consequat, nibh ac hendrerit posuere, lectus augue lacinia turpis, ut imperdiet tortor ligula et massa. Etiam dui neque, convallis at semper eu, porta a magna. Nulla facilisi. Donec convallis pellentesque imperdiet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed ultricies, turpis eu commodo aliquet, diam mauris mattis neque, sit amet volutpat ligula leo id dolor. In hac habitasse platea dictumst. Etiam sit amet mollis justo.</p>'),
(6, 6, 'fr', 'online', '2013-02-24 16:37:28', 'Tu honoreras ta mère et ta mère', 'Cinéma Star', 'image6.jpg', '20h15', '', '<h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h1>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris iaculis cursus diam sit amet cursus. Duis eget nibh ut felis lacinia iaculis eu in enim. Morbi feugiat convallis ante. Vivamus non sodales ante. Fusce nec nisl augue, ut volutpat est. Praesent consequat, nibh ac hendrerit posuere, lectus augue lacinia turpis, ut imperdiet tortor ligula et massa. Etiam dui neque, convallis at semper eu, porta a magna. Nulla facilisi. Donec convallis pellentesque imperdiet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed ultricies, turpis eu commodo aliquet, diam mauris mattis neque, sit amet volutpat ligula leo id dolor. In hac habitasse platea dictumst. Etiam sit amet mollis justo.</p>'),
(7, 5, 'fr', 'online', '2013-02-24 16:37:28', 'Le Restaurant Savoyard', 'Etoile des neiges', 'image5.jpg', '19h00', 'Etoile des neiges', '<h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h1>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris iaculis cursus diam sit amet cursus. Duis eget nibh ut felis lacinia iaculis eu in enim. Morbi feugiat convallis ante. Vivamus non sodales ante. Fusce nec nisl augue, ut volutpat est. Praesent consequat, nibh ac hendrerit posuere, lectus augue lacinia turpis, ut imperdiet tortor ligula et massa. Etiam dui neque, convallis at semper eu, porta a magna. Nulla facilisi. Donec convallis pellentesque imperdiet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed ultricies, turpis eu commodo aliquet, diam mauris mattis neque, sit amet volutpat ligula leo id dolor. In hac habitasse platea dictumst. Etiam sit amet mollis justo.</p>'),
(8, 3, 'fr', 'online', '2013-02-24 16:37:28', 'Visite guidée gratuite de Strasbourg', 'Local Trotter', 'image3.jpg', '14h00', 'Local Trotter<br/>\r\nPlace de cathédrale', '<h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h1>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris iaculis cursus diam sit amet cursus. Duis eget nibh ut felis lacinia iaculis eu in enim. Morbi feugiat convallis ante. Vivamus non sodales ante. Fusce nec nisl augue, ut volutpat est. Praesent consequat, nibh ac hendrerit posuere, lectus augue lacinia turpis, ut imperdiet tortor ligula et massa. Etiam dui neque, convallis at semper eu, porta a magna. Nulla facilisi. Donec convallis pellentesque imperdiet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed ultricies, turpis eu commodo aliquet, diam mauris mattis neque, sit amet volutpat ligula leo id dolor. In hac habitasse platea dictumst. Etiam sit amet mollis justo.</p>');

-- --------------------------------------------------------

--
-- Structure de la table `doc_master`
--

CREATE TABLE IF NOT EXISTS `doc_master` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(50) CHARACTER SET utf8 NOT NULL,
  `type` set('billet') CHARACTER SET utf8 NOT NULL DEFAULT 'billet',
  `place_cat` int(10) unsigned NOT NULL,
  `exp_cat` int(10) unsigned NOT NULL,
  `exp_scat` int(10) unsigned DEFAULT NULL,
  `author` int(10) unsigned NOT NULL,
  `chic_clics` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `target` (`target`),
  KEY `urlKey` (`target`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `doc_master`
--

INSERT INTO `doc_master` (`id`, `target`, `type`, `place_cat`, `exp_cat`, `exp_scat`, `author`, `chic_clics`) VALUES
(1, 'journ-spa-bio', 'billet', 163, 57, 60, 1, 12),
(2, 'cuis-mar', 'billet', 163, 99, 103, 1, 12),
(3, 'vis-grat-stras', 'billet', 163, 99, 100, 1, 0),
(4, 'beau-ling', 'billet', 163, 39, 41, 1, 67),
(5, 'rest-sav', 'billet', 163, 3, 4, 1, 89),
(6, 'tu-hon-ta-mer', 'billet', 163, 94, NULL, 1, 89),
(7, 'drCac', 'billet', 163, 99, 101, 1, 89);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(30) NOT NULL,
  `created` datetime NOT NULL,
  `last_connected` datetime NOT NULL,
  `status` set('online','offline','admin') NOT NULL,
  `connection_serie` int(11) DEFAULT NULL,
  `connection_token` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `label`, `email`, `password`, `created`, `last_connected`, `status`, `connection_serie`, `connection_token`) VALUES
(1, 'Chic-List Admin', 'chiclistadmin', 'test', '2013-02-09 15:45:56', '2013-02-09 15:45:56', 'admin', NULL, NULL),
(2, 'Pierre W', 'orden42@gmail.com', 'test', '2013-02-09 15:46:24', '2013-02-09 15:46:24', 'admin', NULL, NULL),
(3, 'Alfred 1', 'alfred1@test.com', 'test', '2013-02-09 15:47:21', '2013-02-09 15:47:21', 'online', NULL, NULL),
(4, 'Bernard 1', 'bernard1@test.com', 'test', '2013-02-09 15:49:15', '2013-02-09 15:49:15', 'online', NULL, NULL),
(5, 'Charles 1', 'charles1@test.com', 'test', '2013-02-09 15:49:15', '2013-02-09 15:49:15', 'online', NULL, NULL),
(6, 'Denis 1', 'denis1@test.com', 'test', '2013-02-09 15:49:15', '2013-02-09 15:49:15', 'online', NULL, NULL),
(7, 'Edouard 1', 'edouard1@test.com', 'test', '2013-02-09 15:49:15', '2013-02-09 15:49:15', 'online', NULL, NULL),
(8, 'François 1', 'francois1@test.com', 'test', '2013-02-09 15:49:15', '2013-02-09 15:49:15', 'online', NULL, NULL),
(9, 'Gérard 1', 'gerard1@test.com', 'test', '2013-02-09 15:49:15', '2013-02-09 15:49:15', 'online', NULL, NULL),
(10, 'Henri 1', 'henri1@test.com', 'test', '2013-02-09 15:49:15', '2013-02-09 15:49:15', 'online', NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
